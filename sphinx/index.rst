.. lstranslator documentation master file, created by
   sphinx-quickstart on Fri Jun  1 18:13:41 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

lstranslator documentation
========================================

Description
-----------

The **lstranslator** module implements functions to translate a dataset from a format to another one. It is whole implemented in Python 3.

The source can be found here : <https://gricad-gitlab.univ-grenoble-alpes.fr/marinesj/ls_translator>

Those formats are for now:

    * ``WEKA``/``ARFF`` <https://weka.wikispaces.com/ARFF+(stable+version)> (Attribute-Relation File Format) is an file format specially created for describe datasets which are used commonly for machine learning experiments and softwares. This file format was created to be used in Weka, the best representative software for machine learning automated experiments.

    * ``ORANGE 3/4`` <https://docs.orange.biolab.si/3/data-mining-library/tutorial/data.html> is a file format used for Orange software <https://orange.biolab.si/>. It is a famous component-based data mining software including a range of data visualization, exploration, preprocessing and modeling techniques. The two versions (orange2 and orange3) are managed by the translator.

    * ``UCR`` file format.

    * ``GDF`` file format, file . (spreasheet and json mode)

    * ``Old`` file format (almost obsolete)

In same time it implements a new data format ```GDF``` to manage all kind of dataset that would be used in machine learning.
```GDF``` allows to represent the data sets (i.e learning, validation, test, …) used in the learning & visualization tools developped by the LIG for IKATS project.

.. toctree::
   :maxdepth: 2
   :caption: User Documentation

   ./_pages/installation

   ./_pages/commandline

   ./_pages/examples

   ./_pages/api

   ./_md/dataformat.md

Version
-------
``0.46``

Contributors
-------------
It was created in the Grenoble Computer Science Laboratory (LIG), Grenoble, France.

* Julien Marinescu
* Gilles Bisson
* Sami Alkoury
* Vera

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
