-----
# General Data Format (GDF)

-----

Date: 18th april 2018
Revision: V 0.46
Author: G. Bisson, J. Marinescu, S. Alkhoury
Project: IKATS

|Changes done with respect to version 0.42  | |
|Description |Sections concerned |
|-|-|
| Update and complete introduction part and the State of the art of Time Series representations. | 1, 2.2, 7 |
| Introduce a new type "*date*" in order to avoid confusion with the type "*timestamp*" whose use is reserved for sequences only. | 4.1 |
| The type "*timestamp*" now accept to have some *range* restriction. | 4.1 |
| The property "*inlined*" for the sequences has been transformed into a meta information since it doesn't concern the semantic of the sequences. | 4.2 |
| The property "*weighted*" for the sequences has been transformed into a *meta information* since it mainly concern the learning process.| 4.2 |
| A new meta information "*derivative*" for the sequences has been added. It allows to indicate that a sequence is the n-th derivative of another one. This information mainly concern the learning process.| 4.2 |
| Improve the description of the need to handle correctly "unknown" and "don't care" values in GDF format. | 5.3 |
| Modification to deal with the multi-lines data format (DAF) often used in Data analysis. | 5, 7|
| Modification in section 6 and 7 in order to solve some ambiguities in the descriptions. | 6, 7 |
| Correction of many typos as usual. | |


## 1. Context and objectives

This document describes a General Data Format (GDF) allowing to represent the data sets (i.e learning, validation, test, ...) used in the learning & visualization tools developped by the LIG for IKATS project. Along with the classical ordinal and nominal variables, this format allows to represent datasets containing Time Series (TS) and more generally ordered sequences of values. Furthermore, it allows to store the discriminant patterns (i.e. small regular expressions) that can be extracted from the sequences. Here these patterns are produced by HARP algorithm (Heuristical Approach to Research Patterns). Other possibilities allowing to improve the representational capacities of GDF would be introduced in future releases, preliminary versions of these new options are discussed in the different annexes of this document:

- Annex A : Definition of composite types (i.e combinations of basic types),
- Annex B : Notion of *object* allowing to express relational information between observations,
- Annex C : Storage of hierarchical clustering results using extensions proposed in Annexe B.

Ultimately the goal of this format would to be  to express most of the data used in ML and in some cases to represent the results produced by the learning systems when they correspond to an "enrichment" of the initial data. When defining such kind of general format there are several requirements to fullfilled [as explained in this page]
(https://www.quora.com/What-are-standard-formats-for-sharing-machine-learning-data) data format should be: 

* Rich enough to represent and defined data types and application domain dependant information.
* Compact enough to not waste storage for large data sets.
* Be readable by various classical learning tools and by human being.
* Parts of the data can be transferred independently and easily to another format.
* Conversely, the other classical format must be simple to import or transform.
* Be textual as to enable munging using system/shell tools.

At first glance, defining a new representation language seems a little bit useless since, as we will see in section 2, many "general" data formats already exist. However, these formats were mainly defined to represent the "learning set" of the current learning tools and are generally 1) poor in terms of what is expressed since they limit themself to only represent information directly used/usable by these tools, 2) consequently very few information is retained concerning the semantics of the data and more generally about the specificities of the application domain and 3) these formats are mainly seen to represent "input files", forgetting that in some cases datasets can be seen as the output of previous learning processes. Thus, in the context we are pursuing three goals:

* Allowing *to gather multiple input data* having complementary meaning and semantic into an unified data format.
* Allowing *to simplify coding step of the data* by providing extended representational features.
* Allowing this format *to convey some learning results* from one tool to another to create complex dataflows.

We need to emphasize that our goal is not to represent any kind of results/models produced by a learning process, but just those that can be seen an enrichment of the input data. A typical example of that use is are clustering algorithms, such K-means, whose output can be seen as an addition of a new attribute(s) in the learning set expressing observations' category. In this respect, our goal is different but complementary of work like [Predictive Model Markup Language](http://dmg.org/pmml/v4-3/GeneralStructure.html) or more recently [Portable Format for Analytics](http://dmg.org/pfa/docs/motivation/) allowing to describe and exchange predictive model produced by statistical or Machine Learning tools.

In this document, in parallel of describing semantical information managed with GDF, we are focusing on the notion of "text file" representation since they is the most portable and general way to store data. Nevertheless it is would be very easy to have a "database" version of the format in which all of pieces information will be managed by different tables.

## 2. State of the art

#### 2.1 Classical data format 

In Machine Learning (or in Data Analysis) a set of observations, classically named *Learning Set* (LS), is generally expressed as a table (matrix) in which rows represent observations (examples) and columns features (variables). Concerning the file format to use to represent this table into a file there are several possibilities, among the most popular we find:

* Pure CSV file in which the semantic of the features are mostly described with a text document independently from the dataset itself as in the [UCI](https://archive.ics.uci.edu/ml/index.php) repository. Most of the classical ML libraries (scikitlearn, Panda, R, ...) are able to use this kind of representation along with some tools to perform (complex) modifications or filtering while reading the data.
* ARFF files used by [Weka]((http://www.cs.waikato.ac.nz/ml/weka/arff.html)) which allows to declare the features (type and nominal range) used in the data set.
* [Amazon ML format](https://docs.aws.amazon.com/machine-learning/latest/dg/creating-a-data-schema-for-amazon-ml.html) also allows to declare the type of the attributes using a "Data Schema", the data themself being classically written using a CSV file.
* [Orange format](https://docs.orange.biolab.si/3/visual-programming/loading-your-data/) that is based either on three (old version) or one (new version) header lines in the CSV file.

ARFF, Amazon and Orange formats are interesting but in all cases provide somewhat limited information about features' contents and their semantics. Indeed, the set of types proposed is limited to the very standard ones (number, enumerate, string,...) that are closer to computer language types than a genuine way to represent semantical information. In ARRF format is it also possible to declare the range for nominal values. In GDF we propose to manage a larger set of possibilities.

#### 2.2 Representation of Time Series

To represent TS there is no universal format. In practice, as explained in the table below, data structure in  CSV files can be done following three main approaches depending on whether data are mainly organized around the notions of **features** or **examples** or both of them. In the two first approaches, TS can be described in the files using either a **row format** or a **column format**. In GDF, in order to help the user to reuse existing datasets, we are able to deal with all of these representations (see section 6).

| Data structure | Each File contains | Organization of the different files | Observation/Exemple |
| ------------- | :-------------: | :-------------: | :-------------: |
|**Features based** | One pair example/feature | Features are folders containing one file per example, this file contain one TS that can be described either with row or column format | Initial proposition of CS ? |
|  |N examples per feature | Associated to each feature there is a file describing all the examples, in this file TS are oftenly described using row format | [UCR data format](http://timeseriesclassification.com/index.php) (only one feature in this case) moreover, class of the TS (supervised learning) is provided at the beginning of each row |

|**Examples based** | One pair feature/example | Examples are folders containing one file per feature, this file contain one TS that can be described either with row or column format | -
|  |N features per example | Associated to each example there is a file describing all the features (assuming all of them are based on the same timestamp), in this file TS are oftenly described using column format | In [Orange data format for TS](http://orange3-timeseries.readthedocs.io/en/latest/), each column (feature) has an header. The same with Airbus 2 with 4 columns: 2 timestamps, ground speed and heading |

|**Combined** | All data are organized into just one file | The file is structured by block of several lines (features or examples). A classical representation used in statistics is to store from line 0 to N-1 the first example, N to P-1 the second one, etc. In this representation columns are the features. | This is a classical representation in Data Analysis, here we call this format "DAF" |

Regarding the way events (i.e. values) composing the TS are *indexed* there is two situations according to the role of the timestamps:

* **First case**: timestamps are not relevant and/or events are evenly spaced (regular sampling done by a sensor for instance). In such cases, according to the file organization, events are *implicitly indexed* by an integer that is either corresponding to the line number (column format) or the position in the row (row format).

* **Second case**: timestamps are meaningfull and/or events are unevenly spaced (for instance when recording human activity). In such cases, the timestamp values/scale *must be explicitly* provided and coded in the data files. According to the file organization see above, there are two possibilities:

	* *Column format*: one (or several) specific column (i.e. feature) contains the timestamp values.
	* *Row format*: a TS or sequence is composed of a list of pairs \<timestamp, value>. This special format will not be used in GDF to store the temporal data. However it is possible to store the sequences and their associated timestamps into different files using the row format (see section 6 for more details).
	
Finally, it worth notice that in the previous table, column based representations often use an *explicit timestamps*, expressed into one or several columns of the file, and the line based representations often use an *implicit index*, corresponding to the ranks of the values in the sequence.

#### 2.3 Representation of sequences

The notion of TS can be easily generalized to the notion of *sequences* corresponding to a series of ordered values. In such case, the values of the sequence can have also either an *implicit index* based on the position of the values in the seqeunce or an *explicit index* based on any ordered scale (distance, temperature, ...). 

As for the TS, this kind of data is very classical. For instance, that is the case of a DNA sequence in which each "base" (among A, T, G, C) can be indexed by its codon number. Moreover, in some cases, each value of a sequence can be indexed by multiples correlated scales: for instances, in geology, to describe a [*core sample*](https://en.wikipedia.org/wiki/Core_sample) (une "carotte" in french) each values can be indexed both by the distance to the ground and by the corresponding geological time.

## 3. Definition and organisation of a dataset

In the current proposal a dataset is composed of three parts: 

* A **profile** describing the semantic of all features (i.e variable, TS/sequences, patterns, etc),
* A **data table** using the classical matrix format observations/features,
* A **set of files** describing TS or more generally sequences that doesn't fit into the data table.

In this document, the characteristics of a the profile are based on  [JSON](https://fr.wikipedia.org/wiki/JavaScript_Object_Notation) format. For the other files we can use a classical csv file. Of course others, more human friendly possibilities, exist such as [YAML](https://fr.wikipedia.org/wiki/YAML). However, as the hereunder format is flat and homogeneous enough, another solution to ease writing/editing for a human being, is to allow the user to code the full profile using a classical spreadsheet. Such approach , base on using XSLS or ODF formats is explored in Annex D of this document.

## 4. Description of the dataset : profile part

The profile is composed of two parts: 

* A *header* part providing some general (meta)-information about the source of the dataset. Let's notice that we use here a very simplified/modified version of the [Dublin Core](https://en.wikipedia.org/wiki/Dublin_Core). It would be interesting to discuss if more compatible version would make sense or if the current version is enough.
* A *dictionary* part describing the set of features used in the Learning Set, this dictionary being divided into three families of componants: variables, sequences and patterns. Of course, a dataset can contain just one or all of these families (see also Annex B introducing an extension to deal with multiple dictionaries).

In the rest of the description, the property with a leading "\*" are always **mandatory**.

```javascript
header : {
	*doctype: ['GDF', '0.46'],			// representation language and version
	*date: 'yy-mm-dd hh:mm:ss',			// creation date of the current file
	*dataset: 'official name of the dataset',	// id of the dataset
	*source: 'references/origin of the dataset",	// references to the data 
	authors: 'name of the persons who collected data", // references of the authors
	learningSet: ['learningset1', ...] 	// learning set(s) used to generate this one
	}
	
dictionary: {
	variables:	[ ... ]		// List of the "classical" variables (i.e. mono-valued)
	sequences:	[ ... ] 	// List of the TS or sequences
	patterns:	[ ... ]		// List of the patterns and their associated sequences
}

```

* **doctype**: identify the representation language (here GDF) and the version used.

* **date**: date of creation of the file. This is useful to keep track of the initial creation date when the data are copied on different support. We follow a subset of norm [ISO 8601](https://fr.wikipedia.org/wiki/ISO_8601).

* **dataset**: official name (ID) of the dataset. 

* **source**: information about the dataset and identifier (URL, ISBN, ...) allowing  to get more information 

* **authors**: names of people who collected and/or prepared the dataset 

* **learningSet**: List of the learning set (in the current format) that have been used to generate this file. Of course this information only makes sense when this data set is the results of a workflow. For instance in IKATS, HARP tool (pattern generator) reads one or several LS and produces a new LS containing the patterns that have been generated. Thus, the reading procedure of the learning tool could be (optionally) able to read recursively these learning sets while reading the current one, the "full" information being providing by the merge of all the files (see section 7). 

#### 4.1 Variables section

Variables are corresponding to the *classical features* that are used in Machine Learning to describe a simple quantitative, ordinal or quantitative value (i.e. not a Time Series, sequences or patterns).

```javascript
variables: [
	{*name: 	[var1, var2, ...],
	 *type: 	'number|boolean|timestamp|string|enumerate|...',
	 *domain:	'integer|float|...'
	 range: 	[ ...],
	 units:  	'm',
	 relevance:	2.0		// the relevance is expressed as a float, default value is 1.0
	 comment:	'other information about these variables',
	 meta:		[['class'] ['id'] ['ignore'] ...]
	}, ...]
```

* **name**: variables names. Here we can define the characteristics of several variables at the same time when all of these variables share the exactly same definition.

* **type**: allows to declare the GDF type of the values stored in the variable (see hereunder table)

* **domain**: allows to declare the way values are represented in the computer. For instance, with GDF type *number*, values can be represented by an *integer* or a *float* (other possibilities could exist such as: complex, probability, ...). This property is mandatory with **with three exceptions** for GDF types : Boolean, String and Picture whose domain is "self defined".

* **range**: allows optionally to express the possible/relevant values of the type according to its domain. When the range is missing any value compatible with the domain are acceptable. The range is mandatory only for the following GDF types: modular, ordered, hierarchy and enumerate. Beyond the semantical aspect, knowing the range a value is relevant to control de input data and in some ML processes for instance to normalize a similarity.

Properties *type*, *domain* and *range* are in most of the case correlated and allows GDF to represent the three main families of values whose semantic is classical in Data Analysis, that is to say:

* **Quantitative values**: these values allow to measure or to count some quantities. In GTD, types *number* and *modular* belong to this family. Clearly many other types (or domain) could be defined such as interval or normal distribution to represent data expressing some variability but these representation are more used to express set of data rather individuals.
	* **Number** type can be either *float* or *integer* or a restricted interval of these sets. With floating number it is moreover possible to declare the *precision* (i.e number of decimal) of the value.
	* **Modular** type is a reference to the [*modular arithmetic*](https://en.wikipedia.org/wiki/Modular_arithmetic) in which values are represented modulo a given constant. These kind of data are very classical, the most classical examples being the way time is handled modulo 24 (the value following 23h being again 0h) or the measure of an angle in degree which is done module 360. This type of data must be treated in a specific way: for instance, with a learning tool using a distance measure on an angle, distance (360, 0) is smaller that distance (360, 350). Let's notice that [*Circular statistics*](https://en.wikipedia.org/wiki/Directional_statistics) is a relatively new domain of statistics (~1980) dealing with this kind of data.
	
* **Ordinal values**: these values allow to rank some information or to express a preference. In GTD, types *date*, *timestamp*, *mark* and *ordered* belong to this family.
	* **Date** type allows to represent a date. However, this type is not intended to represent a timestamp of a time series, this is the role of the next type. We accept two formats widely used : [Unix time](https://en.wikipedia.org/wiki/Unix_time) and [iso 8601](https://en.wikipedia.org/wiki/ISO_8601)
	* **Timestamp** type allows to represent any "time mark" of a temporal sequence. The format can be either a the one of a real date or a numerical value indicating any period of time.
	* **Mark** type is a generalization of the notion of "scale" that we already have in the timestamps. It can accept any kind of units (time, distance, temperature, ...).
	* **Ordered** type allows to describe a list of numbers or symbols. When the representation use a list of N symbols, their underlying semantic is similar to the one of a list of integer from 0 to N-1. Thus it is for instance possible to compute a numerical distance between two any symbols.
	* **Hierarchy** type allows to represent a hierarchy (tree) of symbol. Thus operation of generalization and specialization are possible and a learning tool can compute a distance, based on the length of the path, between two any symbols.

* **Qualitative values**: these values allow to express some properties or to provide data. In GTD, types *boolean*, *string*, *enumerate* and *picture* belong to this family. 
	* **Boolean** type expresses a boolean value, thus domain and range are totally defined .
	* **String** type expresses a string with a possible control on the maximum length.
	* **Enumerate** type allows to describe a set of symbols.	
	* **Picture** allows to express a filename containing a picture or photo.
	

The following table sums up the basic types and their associated domain and range along with some examples. 

| GDF Type  	| Possible domain| Example of range | Example of value |
| :------------- | :----------------- | :-------------- | :------------- |
| number	| <ul><li>integer</li><li>float</li></ul>| <ul><li>*none*</li><li>[-3, 16]</li><li>[3.14, 18.0]</li></ul> | <ul> <li>512, 2396.425, 3.45e+12 </li><li>-2</li><li>5.67</li> </ul>|
| modular	| <ul><li>integer</li><li>float</li></ul>| <ul><li>[0, 23]</li><li>[-180.0, 180.0]</li></ul> | <ul> <li>10</li><li>-45.76 </li> </ul>|
| date	|<ul><li>[unix_time](https://en.wikipedia.org/wiki/Unix_time)</li><li>[iso_8601](https://en.wikipedia.org/wiki/ISO_8601)</li> </ul> |<ul><li>*none*</li><li>[0, 250000]</li> <li>["1 jan 2012", "31 dec 2012"] </ul>|<ul><li>125000</li><li>"23 jan 2012 17:56:00"</li> </ul>|
| timestamp	|<ul><li>integer</li><li>float</li><li>[unix_time](https://en.wikipedia.org/wiki/Unix_time)</li><li>[iso_8601](https://en.wikipedia.org/wiki/ISO_8601)</li> </ul> |<ul><li>*none*</li><li>[0, 4000]</li> <li>[0.0, 60.0]</li><li>[1200000000, 2400000000]</li><li>["1 jan 2010", "31 dec 2017"]</li> </ul>|<ul><li>2308</li><li>23.614</li><li>1270990000</li><li>"23 jan 2012 17:56:00"</li> </ul>|
| mark		| <ul><li>integer</li><li>float</li></ul>| <ul><li>*none*</li><li>[0, 10000]</li><li>[-273, 10e9]</li></ul> | <ul> <li>10, 34.43 </li><li>3</li><li>4.56e6</li> </ul> | 
| ordered	| <ul><li>integer</li><li>string</li></ul>| <ul><li>[5, 6, 7, 8]</li><li>["small", "medium", "large"]</li></ul>| <ul><li>8</li><li>"medium"</li></ul>| 
| hierarchy	| <ul><li>string</li></ul> | ["size" ["small" ["xs","s","m"]] ["large"["l","xl","xxl"]]] |Ê"m"| 
| boolean	| N/A	| -	| true| 
| string	| N/A	| <ul><li>*none*</li><li>10 (*string max length*)</li> </ul>| <ul><li>"Once upon a time ..."</li> <li>"Very short"</li></ul>| 
| enumerate	| <ul><li>string</li></ul>	| ["tv", "radio", "web", "paper"]	| "web"| 
| picture	| N/A		| - | "face45.jpg" | 

* **precision** allows to express the precision of the float number (when used) for types: number, modular, timescale and mark.

* **units** allows to express the unit of the variable when this information makes sense. When possible, this information should be coded in order to be compliant with [International System of Units](https://en.wikipedia.org/wiki/International_System_of_Units) notation. For a timestamp, unit is by default the second "s" but can be redefined by the user.

* **relevance**: semantical relevance or preferences (expressed as a numerical weigth) associated to the current variables. This information can be used for visualization purpose (display order of the variable) or during the learning process, for instance: in a distance based learning tool (for instance, in k-NN algorithm) to weight the importance of a variable, or to solve a "draw" between some variables in a decision tree. When missing, *Default value* for relevance is 1.0 (floating number).

* **comment**: description of the role of the (set of) variable(s) that can be used in the interface to explain some information about the meaning of a result for instance.

* **meta**: List of meta information indicating the role of the current variables mainly according to the learning tasks. Each meta information is a list ['keyword', 'value1', 'value2,], however values are optional in many cases. The list of meta information for the variables is given in the hereunder table but this list can be extended in the future. The most generic solution is to consider this field as a list of "open" pairs [keyword values] whose information are used (or not) by the learning tool.

| Keyword	| Semantic of the meta-information | 
| ------------- | -------------------------------- | 
|*[CLASS]*Ê	|  Indicates that all the variable(s) in the block express the class to be predicted (supervised learning). Classically, there is only one variable having this metadata but it is possible to declare several times in the LS that a variable is a class when we want to do a multi-target classification. *Let's notice this keyword is equivalent to "c: class attribute" in Orange format*.|
|*[ID]*		| This indicates that the content of the variable(s) are used to "name" the current example when reading a data table. If several variables have the ID metatag the name of the example is the concatenation of all these values separated by an underscore. Of course, learning/viz tools are free to internally use or not this information to name the examples. Let notice that declaring a variables as an ID imply that it is not used during the learning (imply IGNORE meta). When no variables are declared with an ID metatag, the loader classically uses the line number of the examples in the dataset has its ID.|
|*[IGNORE  \<value1>, ... ]*	| All variable(s) of the block or just those in the list are not explicitly used to learn. Nevertheless, they are loaded into memory. *Let's notice this keyword is equivalent to "m:meta attribute" in Orange format*.|
|*[SKIP  \<value1>, ... ]*	| All variable(s) of the block or just those in the list are not loaded into memory. *Let's notice this keyword is equivalent to "i: ignore attribute" in Orange format*|
|*[STRENGTH]*	| Indicates that this variable is used to set a weight to the observations (examples) in the data tables. Classically the weight is coded as number but in GDF we make no assomption about its type. This meta information *must appear only once in the profile*. This information can be used by the learning tool to focus the learning process on some observations.*Let's notice this keyword is equivalent to "w: instance weight" in Orange format*|
|*[PROTOTYPE]*	| Indicates that this variable is used to indicate that an observation in the data tables is a *prototype*. This notion is  relevant if GDF file is the result of a clustering algorithm: a *prototype* is an observation which is a good representative of a cluster (typically the *centroid* of K-means). Classically the prototype property is coded as a boolean but in GDF all possible types can be used. As for the weight this meta information *must appear only once in the profile*.|

#### 4.2 Sequences section
 
This section allows to describe the sequences or TS that appear in the data table. Most of the properties are identical to the ones found in the previous section to describe the variable, but there are two kinds of information indicating the way values of the sequence are *indexed* (timestamps or marks) and *sampled*.

```javascript
sequences: [
	{*name:		[seq1, seq2, ...],
	 *type:		'number|boolean|timestamp|string|enumerate|...',
	 *domain:	'integer|float|...'
	 range:		[ ...],
	 units:		'kg',
	 *index:	[timestamp_var|mark_variable]|'integer',
	 *sampling:	either a number, 'uneven' or 'unknown',
	 relevance:	2.0		// the relevance is expressed as a float, default value is 1.0
	 weighted:	'true|false'
	 comment:	'other information about these sequence',
	 meta:		[['ignore'] ['skip'] [inline]]
	}, ...]
```

* **name**: list of sequences names.

* **type**, **domain** and **range**: type of the values composing the sequences values and their range. The list of possible types is the same as for the variables (see section 4.1).

* **units** allows to express the unit of the sequence values (see section 4.1). There is no semantical verification of the correctness of the unit. Thus when the role of the sequence is "derivative" (see below) there is no checking that the unit is in coherence with the derivation done.

* **index** allows to declare the scale associated to the sequences of the current block. **Caution**: a sequence declared as *timestamp* or *mark* cannot have an index since THEY ARE the indexes. There are two possibilities of index:
	1. either the time series use an **implicit numerical** index (row number or column number according to the representation format, see sections 2.2 and 6) to ordered the events: in this case the value is the string "integer".
	2. or the time series use one or several **explicit index** represented by others sequences (i.e. a list of timestamps or marks) and we need to provide *the name of these sequence* expressing this scale. LetÕs notice that these sequences must **have been declared previously** in the profile and the type of their values must be equal to either "timestamp" or "mark". In the data part all the sequences must have a number of values equal or smaller: in other term an index can be longer that the sequences using it but not shorter! 

* **sampling**  allows to indicates the sampling rate (or more generally the precision) of the sequence when the data are coming from a sensor or repeated measures. **Caution**: this property only concerns sequence of type *timestamp* and *mark* (i.e. sequence that will be used as an index). There are three possibilities of sampling:

	1. The sampling is regular (synchroneous) and we can provide the sampling rate. Let's notice that the **unit** used to express the sampling rate is the same as for the values. 
	2. When the sampling is asynchroneous (for instance if we have recorded user events on a time scale) this field will contains the string 'uneven'. 
	3. Finally, it is possible to indicate that the sampling rate is just 'unknown'. 

* **relevance**: semantical relevance/preference (i.e. weigth) associated to the current variables (see section 4.1 for more details). *Default value* for relevance is 1.0 (floating number).

* **comment**: description of the role of the sequence or the Ç long name È that can be use in the interface to comment a result.

* **meta**: List of meta information indicating the role of the sequences for the learning task. Generally when the option "weight" is used (see above) for a given sequence SEQ, it would be interesting to add automatical the meta information ['ignore', 'SEQ.weight'] to indicate to the learning tool that this information is not to used as a "real" feature during the learning step.

| Keyword	| Semantic of the meta-information | 
| ------------- | -------------------------------- | 
|*[IGNORE  \<value1>, ... ]*	| All sequence(s) of the block or just those in the list are not explicitly used to learn. Nevertheless, they are loaded into memory.|
|*[SKIP  \<value1>, ... ]*	| All sequence(s) of the block or just those in the list are not loaded into memory.|
|*[INLINED  \<value1>, ... ]*	| Indicates that when writing a datatable, all sequence(s) of the block (or just those in the list) must be "inlined" into the table (see section 6.1) rather than written in an external file. This option is interesting when the sequence are very short ansd that the want to be able read it directly. This option is taken into account only when a data file is written (section 7) during the reading process the rules expressed in section 6 are used.|
|*[WEIGHT \<sequence>]*	| Indicates that this sequence is vector of weights associated to a another *sequence* S  allowing to asset an "importance" to each value of S. The lenght of current sequence must be the same than S otherwise a reading error is raised. If a vector of weights is missing (see section 5.1) we assume that this vector is uniform and composed of 1.0. |
|*[DERIVATIVE \<N-th> \<sequence>]* | Indicates that this sequence is a *N-th* derivative of a another *sequence* S. This information can be useful for some learning tools that are dealing with a value and its derivative at the same time (typically the case of HARP in IKATS). There are also two constraints: 1) the *index* (timestamps or marks) and *sampling* information must be the same as S and 2) the lenght of current sequence must be the same than ()\|S\|-Nth). If one of these two conditions is false a reading error is raised. |

 #### 4.3 Pattern section

This section allows to describe the patterns (i.e a small part of discretized TS or sequence) that have been generated with other discovery tools (for instance, HARP in IKATS project) or even generated by hand by the user. From a learning point of view, patterns are very similar to the variables, but the way they are processed are totally different. Indeed, while a variable is directly provided in the data table, a pattern must be "pre-computed" at the runtime to retrieve in the new set of sequences *if* and *where* this pattern occurs and to compute all the information needed to fulfill the *indicators* (see below). Thus, we need to provide some pieces of information to allow this dynamic exploration.
 
```javascript
patterns: [
	{*names:	[pat1, pat2, ...],
	 *generator:	['HARP' ['version', 1]],
	 *definition:	['[AB].$H[EF]', '[AB][CD]$.[BC]', ...],
	 *sequences:	['speed', 'angle_dr'],
	 slice:		[start_point, end_point],
	 indicators:	[exist, count, fst_pos,...],
	 breakpoints:	[[-5.6, -4, 0, 2], [-2, 5, 12, 19]],
	 vocabulary:	[[], ['freeze','cold','fresh','mild', 'warm']],
	 relevance: 	2.0		// the relevance is expressed as a float, default value is 1.0
	 comment: 	'other information about these patterns',
	 meta:		[['class'] ['id'] ['ignore'] ...]
	}, ...]
```
* **name**: pattern names. Let's notice that theses names must correspond (as for the variables and sequences names) to a readible description of these patterns. In other terms, these names must be as explicit as possible about their meaning.

* **generator**: name of the tool that is able to parse and to execute the patterns with: its version and the (optional) list of parameters used to learn the patterns each argument of this list is a pair [name, value].

* **definition**: "black box" part containing the full description of the patterns using the language used by the generator. In the case of HARP the language is close (but not identical) to a simplified version of Regex but in theory any kind of description can stored here. This part can also contains the arguments used by the generator to built the patterns.

* **sequences**: sequences associated to the patterns, a pattern can correspond to one sequence or to code a correlation between two or more sequences. Theses sequences must **have been declared previously** in the profile. This is important since they allows to know the type, domain and range on which the patterns are generated. There are two possibilities:

	* When type equals to 'number', 'modular', 'timestamp' or 'mark', the values can be discretized and then the properties **breakpoints** (and optionally **vocabulary**) are necessary to the discretization processes.
	* When type equals to 'boolean', 'string', 'enumerate', 'ordered' or 'hierarchical', the values are already discretized (by definition) and properties **breakpoints** and **vocabulary** are useless. A warning must be generated by the reading procedure if they are provided in the description.

* **slice**: indicates that the patterns concern only a subpart of the sequences. The starting and ending point are expressed in the units of the first sequence (assuming all the sequences are aligned). If this property is missing the all values of the sequence are taken into account and explored to find the patterns.

* **indicators**: when HARP generates a pattern it is also able to create a set of new variables about it: for instance the number of occurrences of the pattern, its average position, the gap between two occurrences, etc. To avoid to have to declare in the profile part all of these descriptors, we just provide here a list of these "indicators". For instance, if in a block containing the patterns ['P1, 'P2] we declare the indicators ['exist', 'min_gap'], this means that two descriptors will exists in the data table for all the patterns: 'P1.exist' 'P1.min_gap', 'P2.exist' and 'P2.min_gap'.
There is also a very special "indicators" named "pos" coding for a sequence and not a value. It contains all the occurrences (i.e. positions) of the current patterns in the observations with two possible format:
	* index1, index2, ... when the pattern have a constant size (default case). When the sequence has more than one index only the first one is used in this list.
	* [index1, length1], [index2, length2], ... where length is the length of the pattern. This information makes sense when the size of the pattern is not a constant, for instance with a regex as "B[A]+C" whose length depends on the number of letter "A" found in the sequence.
	
**Important**: to avoid to have a separate file for each observation it seem logical to use the format "row based representation" (see section 3.2) to store this information !

Here is the list of current indicators for HARP and their types (this list will be possibly extended/modified !). When property *indicators* is missing of the pattern definition, column 'exist' is automatically generated in the data table.

| Indicators    | domain    | Semantic   |
| ------------- | ------- | ---------- |
| exist		|boolean  | indicates if the pattern exist in a sequence. |
| count		|integer  | number of occurrences. |
| fst_pos	|index    | position of the first occurrence. |
| lst_pos	|index    | position of the last occurrence. |
| avg_pos	|index    | average position. |
| min_gap	|index    | smaller interval between two consecutive occurrences. |
| max_gap	|index    | larger interval between two consecutive occurrences. |
| avg_gap	|index    | average interval between two consecutive occurrences. |
| pos	  	|sequence of positions| list of the occurrences of the patterns. |

* **breakpoint**: List of breakpoints associated to each sequence allowing to discretized the numerical values (not needed when 1) a sequence is made of nominal values and/or 2) the generator is not based on a discretization of the values). There are 2 important things to notice. Firstly, in GDF each set of patterns has its own list of breakpoints, meaning that for a same seqeunces several discretizations are possible. Secondly, the first and last values of a list of interval in the breakpoints are "open" and only bounded by the **range** of the sequence. For instance the list of breakpoints [-2, 0, 2]:
	* **means** : there are four intervals [min_range, -2[; [-2, 0[; [0, 2[; [2, max_range,]
	* **doesn't mean** : there are two intervals [-2, 0[ ; [0, 2]

* **vocabulary** is used to express the discretized values in a more readable format using all possibilities of Unicode. The number of items in the vocabulary must equal to the *number of breakpoints plus one* since the breakpoints are correspond to the treshold and the vocabulary to the intervals! By default, if this field is missing the generator uses its default representation (e.g. alphabet letters {A, B, C, ...} for HARP), or the real values in the case of discrete values (ordered, string, ...).

* **relevance**: semantical relevance (i.e. weigth) associated to the current pattern. This value is inherited by all the indicators derivate from the pattern. *Default value* for relevance is 1.0 (floating number).

* **comment**: description of the role of the pattern or the Ç long name È that can be use in the interface to comment a result.

* **meta**: List of meta information indicating the role of the patterns in the learning task. Generally when the indicator "pos" is used (see above) for a given pattern P1, it would be interesting to add automatical the meta information ['SKIP', 'P1_pos'] to indicate to the learning tool that this information is (in most of the cases) not to used during the learning step.

| Keyword	| Semantic of the meta-information | 
| ------------- | -------------------------------- | 
|*[IGNORE, \<value1>, ... ]*	| All pattern(s) of the block or just those in the list are not explicitly used to learn. Nevertheless, they are loaded into memory. *Let's notice this keyword is equivalent to "META" in Orange format*.|
|*[SKIP,  \<value1>, ... ]*	| All pattern(s) of the block or just those in the list are not loaded into memory.|


## 5. Description of the data table

The data table contains a set of pairs feature/value allowing to describe the examples. In GDF we accept two different formats for this data table: GDF table and DAF table (see section 2.2). Both of them are based on a classical [CSV coding](https://en.wikipedia.org/wiki/Comma-separated_values) allowing to use the classical "delimiters" (e.g: comma, tab, ...).

In both table formats the **header line** of the file contains the name of the columns to improve readability of the table. Moreover, in this way, the order of the features/column can be different from the declaration order in the profile but of course feature names must be identical and an error must be raised by the parser if not.


##### 5.1 GDF table

This format, which is the default data format for GDF, is classicaly used by most the machine learning tool to describe the dataset. In this kind of table:

* each line is an example/observation
* each column is the value of a feature (variable/sequence/pattern). 

In the case of the sequences, this value is a "pointer" on the file containing the set of values in the case of a TS or sequence (see section 6 for more details). When the format is used along with a database (as in IKATS project for instance), this pointer is an identifier to the corresponding item in the database.

##### 5.2 DAF table

As we saw in section 2.2 another  format is widely used in Data Analysis in which all data, including sequences  are stored in the same file. In this kind of table:

* each example/observation are stored using several lines, the number of lines for each observation being equal to the length of its longuest sequence.   
* as in GDF format each column contains the values of a feature (variable/sequence/pattern). When the features is a variable or a pattern the first ligne of the example contains its value, the other lines can either repeat the same value or being just empty. If a sequence is shorter than the other the lines after the end of this sequence are empty for the corresponding column.

##### 5.3 The case of special values (missing, unrelevant, ...)

Independently from the way the tables are stored (GDF or DAF) there are often some values in the dataset that are either:

* missing or too noisy (named *unknown values*, generally coded ÇÊ?ÊÈ) or
* without any meaning (named *donÕt care values*, generally coded ÇÊ*ÊÈ), in this case it means that the corresponding value is unrelevant for the current observation.

These "special values" can occur in any kind of variable (standard values or part of sequences) and GDF **must be able** to represent them in the files and within memory. A good example of how these values can "appear" is given by the *patterns* part of the profile. For instance, let's imagine that a pattern "A[B]BC" is relevant for a class C1 but that it is not (or rarely) occurring in the other classes. This means that the values of the position/interval based *indicators* of this pattern (e.g. "fst_pos" or "min_gap") can be "undefined", for the observations not belonging to class C1. By coding the information with any other values than *donÕt care* (for instance fst_pos=0,  min_gap=-1) we would just transmit false information to the learning tools and thus could leading them to take a bad decision.

From the learning point of view, the way to deal with them is very dependent of the learning algorithm (but they **don't concern** the reading process). There are some classical strategies, for instance:

* **Unknown values**: to approximate the value by the average value in the database or by the average value of the class to which belong the example in supervised learning, or by the average value of the current node in a decision tree method.
* **DonÕt care values**: to "neutralize" the value by considering all the possible values (but this method is only possible when the feature is a nominal type) or to consider that the corresponding pair observation/value doesn't provide any information.


## 6. Links to the external Sequences

As we saw in the previous section, when the file describing data table use the GDF format, values corresponding to sequences are generally (see exception herunder) "pointers", namely a directory and a filename, to the external CSV files containing these data. As we saw (details in section 2.2), there are two "classical" ways to describe the TS or sequences, to summarize:

* **Row format**: each line describes the TS/sequence associated to a given feature, each file describing one or several observations.

* **Column format**: each column describes the sequence associated to one or several features (thus, number of lines of a file = number of timestamps of the current observation). In this format, when there are several columns, the first line is an header indicating the name of the feature associated to each column.

##### 6.1 The specific case of inlined sequences

Sometime a sequence can be very short containing just 2, 3 items. In this case it is interesting to allows the possibility to "inline" this sequence directly in the data table thus avoiding to create some files. This feature is only possible when the sequence use an *implicit index* (integer). The format for this data is a string beginning with the two characters "@[". For instance: "@[item 1, item 2]", in such case case the different values are separated with a comma.

##### 6.2 Automatic file format detection

As the files describing sequences can come from different "sources" and to avoid the user to have to do a lot of preprocessing, it would be interesting to implement an **intelligent reading process** that would be able to automatically "detect" the different kind of formats (row/column, with/without header, etc) and to process the files containing sequences in a relevant way. We are going to consider all classical situations and look at the way to deal with them.

First, in **row format**, for a given feature (i.e. sequence), a file often described more than one observation, meaning that in the data table, the name of this file will be repeated several times in a same column (feature). That is a problem since we need to assume that the line number describing an example ÇÊnÊÈ is coherent in both files (i.e. data table and external file). However, as soon as the user slightly modify the data table (for instance by sorting observations), this "order based" link will be silently broken. Thus, to have a more robust way to associated the right sequence to the right observation it is important to create an explicit link between the two pieces of information. We propose two different methods: 

1. to associate a line number <nnn> to the name of each file <X> to identify the right sequence. This method has the advantage to need no extra-information.
2. to associate a name (i.e string) to the of each file <X>, assuming that in this file each line is indexed by a name placed on the first column. This second method is more robust.

Second, in **column format**, things are simpler if we assume that the first line always contains an header (i.e the list of feature(s)) as soon as there is more than one column. In the later case we could accept that the header is present or missing.

The next table summarize all possible cases (according to the content of the file in terms of number of lines, columns and presence of a header) when one read an external sequence. It describe how the file "X" must be described in the data table and the action to do with the possible failures. Let's notice that the reading procedure should be able to remove the empty lines or empty columns in the files to be more robust and to avoid raising useless errors.

| #Row, #Col, Header | External file content | Data table syntax | Action ... |
| :-: | :-------------: |  :-------------: |  :-------------: | 
| 1, N, no |one sequence using row format | "X" | Read the sequence of length N|
| N, 1, no |one sequence using column format without any header | "X" | Read the sequence of length N|
| N, 1,Êyes |one sequence using column format with an header | "X" |  Read the sequence of length N-1 and verify that the header name corresponds to the current column of the data table |
| N, P, no |N sequences with row format | "X:\<nnn>" or "X:\<name>" | Read the sequence \<nnn> or \<name>, an error is raised if it doesn't exist|
| N, P,Êyes |N sequences with column format | "X" |  Read the sequence in the column name corresponding to current column of the data table, an error is raised if it doesn't exist |

Finally, once the file format has been determined for a given sequence, the reader process interprets the sequence according to **index** property (see section 4.2) indicating the way to associate properly the index(es) with the values of the sequence. An error is raised when:

* A sequence use an *explicit index* and that the length of the two sequences (index and values) are not equal.
* A feature corresponding to the same index appears into several files and that the different occurrencies are different.

Section 9 provides an exemple of some of the different ways to represent the sequences and their associated indexes.

## 7. Reading and exporting data format

To ease diffusion of GDF, it is very important to provide users with a "translation" tool able to **read** and **write** several classical data format to simplify the convertion into the format describe in this document. The most interesting targets are:

* [UCR format](http://timeseriesclassification.com/dataset.php) (TS data bases)
* [Orange format](https://docs.orange.biolab.si/3/visual-programming/loading-your-data/)
* [Weka (ARFF) format](http://www.cs.waikato.ac.nz/ml/weka/arff.html)
* [Amazon ML format](https://docs.aws.amazon.com/machine-learning/latest/dg/creating-and-using-datasources.html)
* DAF representation (see section 2.2) is widely used in Data Analysis. In this format all data are stored in one file: columns being the features and examples being organize into block of N consecutive lines. This format leads to some odd characteristics complicating a little bit the parsing. For instance as all features have the same number of lines, this leads to have some cells with a repeated or null value in the case of classical variables.
* A **spreadsheet based representation** to help edition of the profile (see annex D). Such format is very interesting since in many scientifical or industrial domain, users are storing/modifying their data using this format (even if the adequacy of this approach is very questionable).

Of course when exporting GDF file into another format, some features are impossible to express. Translation process must be able **to keep as much information as possible**, for instance missing types will be translated into their closest equivalent. Another problem is that in some data format there are "implicit" information that we want to becomes explicit (i.e described as a feature) in our format. For instance, if observations are named in the source file but that this name is not associated to a variable we need to generate a specific variable with two contraints to respect:

* we need to avoid any naming conflict with the other features (one naming space)
* this features must be easily identified when we export to another format to avoid to generate a feature that doesn't previously exist (e.g when we import then export to GDF a data format, the exported file must be semantically identical to the initial one !)

So we use the following convention: features that has been generated due to the conversion process begin with the character "@". For instance "@ID" is a name automatically generated, "@class" the feature containing the class of the observation, etc.

##### Canonical data format for GDF

As we saw in section 6 the sequences stored in different files can have different formats (row/column, with/without header, etc). If the reader process is able to deal with all of them, it is also important to be able, when we export a learning set to GDF to "normalize" the representation of the sequences and to provide the most interesting structure according to the number of observations/features. Thus, two main options must be implemented (corresponding 4 possibilities):

* selection between observations based or features based organization of the files. 
* Selection between mono or multi-sequences files.

Here is a short description of files organization for each case:

| - | Observations based | Features based |
| - | ------------------ | -------------- |
| One sequence/file | There is only one folder per observation, each folder containing as many files than the number of features (excluding indexes); we use column format to store the values and the n indexes sequences (generally n=1) are provided inside the same file leading to a n-columns representation (with n-1 indexes). | There is one folder per feature (excluding indexes), each folder contains as many files than the number of observations, we use column format to store the values and the indexes are provided inside the files leading to a n-columns representation (with n-1 indexes).|
| Many sequences/file | The sequences associated to an observation are described into one or several files according to the number of different index sequences that exist (timestamp or mark). Thus, each file contains values of one index and as many columns as the number of features using this index. |ÊThere is one file per variables, each file contains as many lines than the number of observations. These observations are named using their current ID (i.e. and thus we use of "X:\<name>" format in data table) and explicit indexes are stored as standard variables in other files. This representation is mainly interesting when index are implicit (position based). |

## 8. Merging data files

When we describe a Learning Set, it is sometime useful (or even necessary) to split it into several parts, that typically the case when a tool as HARP "complete" an initial LS by adding some patterns. Thus the LS readers must be able to deal with these situations. In practice we have two cases:

* The LS files shared the same profile (i.e. sames features) and the collection of data are just corresponding to different set of examples. In this case, the resulting data set is just the a concatenation of the data part (i.e merging lines in the data table).

* The examples are described using several profile, each profile corresponding to a subset of the descriptors. In such case the different definition of descriptors can be "concatenated" (i.e merging columns in the data table) but with some cautions to be able to export a correct format:

	* Descriptors of two different profiles must have different names to avoid naing conflict.
	* The resulting header is the header of the first profile (assuming that the list of LS is ordered) and the other part of the profiles are merged together.

## 9. Example of GDF representation

Here we provide a small example illustrating some of the possibilities of sequence descriptions. The profile (see section 4) contains the following declarations:

* OBS is the name of the observation
* V1 is an ordered variable with range [a, b, c, d]
* T1 and T2 are correspond to explicit timestamps
* S1, S2, S3 are sequences of integer, S1 is indexed by T1 and S2, S3 by T2
* P1 is a pattern defined as "A[BC]" with variants *count* and *min_gap*

This will correspond to the following JSON code:

```javascript

{
  "header": {
    "doctype": ["GDF", 0.32],
    "date": "18-02-14 22:10",
    "dataset": "Data example",
    "source": "IKATS project",
    "authors": "G. Bisson",
    "learningSet": []
  },
  "dictionary": {
    "variables": [
      {"name": "OBS",
        "type": "string",
        "comment": "name of the observations",
        "meta": [["id"]]
      },
      {"name": "v1",
        "type": "ordered",
        "domain": "string",
        "range": ["a", "b", "c", "d"]
      }
    ],
    "sequences": [
      {"name": ["T1", "T2"],
        "type": "timestamp",
        "domain": "integer",
        "units": "year",
        "sampling": "uneven"
      },
      {"name": "S1",
        "type": "number",
        "domain": "integer",
        "range": [0, 100],
        "units": "m",
        "index": "T1"
      },
      {"name": ["S2", "S3"],
        "type": "number",
        "domain": "float",
        "precision": 2,
        "units": "cm",
        "index": "T2"
      }
    ],
    "patterns": [
      { "name": "S[ML]",
        "generator": ["HARP", ["version", 1]],
        "definition": "S[ML]",
        "sequences": "S1",
        "indicators": [ "count", "min_gap" ],
        "breakpoints": [10, 25, 65],
        "vocabulary": ["S", "M", "L"]
      }
    ]
  }
}

```

Here is an example of the data table:

| OBS  | V1 | S[ML]\_count | S[ML]\_min\_gap | T1 | T2 | S1 | S2 | S3 |
| --   | -- | ------------- | --------------- | -- | -- | -- | -- | -- |
| obs1 | b  | 1 | 0 | T1-ob1.csv | data1.csv | S1.csv:00 | data1.csv | data1.csv |
| obs2 | c  | 4 | 3 | T1-ob2.csv | data2.csv | S1.csv:01 | data2.csv | data2.csv |
| obs3 | c  | 2 | 6 | T1-ob3.csv | data3.csv | S1.csv:02 | data3.csv | data3.csv |
| obs4 | a  | 2 | 1 | T1-ob4.csv | data4.csv | S1.csv:03 | data4.csv | data4.csv |

As we can see features T1 and S1 are stored into two separate using a row format, while T2, S2 and S3 are stored in the same files using the column format. Here is an example of the content of this files:

For the four files corresponding to T1: 

| File | Values|
| --   | ----- |
| T1-ob1.csv | 23624, 23627, 23700, 23745, 23802|
| T1-ob2.csv | 4567, 4678, 5000, 5005, |
| T1-ob3.csv | 45890, 45895, 46700, 48904, 48950, 48960|
| T1-ob4.csv | 0, 8, 14, 25, 102, 206, 424, 425|

For sequence S1 there is only one file using row format. The number of values on each line must be coherent with content of the files describing the sequence T1.

| S1.csv file|
| ---------- |
| 45, 67, 12, 0, 14|
| 100, 98, 94, 80 |
| 56, 58, 78, 96, 100, 88|
| 67, 56, 45, 54, 78, 100, 98, 86|

For data1.csv using the column format. Files data2, data3 and data4 have a similar structure.

|Data1.csv|||
| T2  | S2    | S3     |
| --  | ----- | ------ |
| 678 | 45.40 | -12.64 |
| 702 | 56.67 | -10.00 |
| 804 | 57.43 | -9.24  |
| 856 | 65.08 | -11.56 |



<div style="page-break-after: always;"></div>

--------
## Annex A : Composite types.

In section 4.1 we described the GDF basic types. This list could be extended, for instance to include complex numbers, probabilities, ... However, in many domains some values are composed of several basic arguments. For instance, to store a geographical position we can use 2D or 3D coordinates (corresponding to 2 and 3-uplets) ; we need the same thing to store some statistical information (mean and standard deviation, intervals, ...); etc. Of course, each value could be stored independently in several variables of the data table but by doing that we lose the meaning of the data and a program that will analyze them have to "rebuilt" the complete information.

Thus the idea would be to add a new block in the dictionary allowing to declare *composite types* that is to say a type of data which is a n-uplet composed of the GDF basic types. Here, we make no assumption on the way the ML tool will use these data and it will be probably interesting to introduce some semantical information to express how these data must be processed (for instance: providing a similary function). 

Here is an example of declaration in the Dictionnary block. In the other part (variable, etc) the user will just use the new type name without any possibility to specialized the definition.

```javascript
type: [
   {name: "coordinate",			// new composite type
    type: ["modular", "modular", "number"],
    domain: ["float", "float", "float"]
    range: [[-180.0, +180.0], [-180.0, +180.0], []],
    units: ["degree", "degree", "m"]
    comment: "Longitude, latitude and altitude"
    },
    { ... }
    ]
variables: [
      {name: "object-pos",
       type: "coordinate",		// we use the composite type as previously defined
       comment: "X, Y, Z observations",
    
```

<div style="page-break-after: always;"></div>

--------
## Annex B : Relational information.

The tabular representation widely used in ML (see section 2.1) is simple but it offers no way to easily express relational information (i.e: datasets in which parts of the observations contain some references to one or several other observations). Here the idea is to propose a simple extension of GDF to deal with this problem.

- Benefit: allow to represent relational information in a dataset
- Basic idea: the profile can contain several dictionaries with some link between them
	- Each dictionary has a name (default name is "data")
	- Each dictionary **has its own table** 
	- A dictionnary can be seen as a new domain of the type "relation".
	- The type of a variable or a sequence could be the name of one (or several) dictionary. In this case that means that we have a pointer on an element (observation) of another table. In the next example, a person can have pointers on a *company* table, a *town* table and even on his own table.

Example of declaration of a relational structure

```javascript
dictionary:[
   person:{				// name of the file containing the table
	  variables:[
	  	{ name: "firstname",
	  	  type: "string"},
	  	{ name: "employer",
	  	  type: "relation",	// relational field 
	  	  domain: "company"},	// value is the ID on an observation in table "company.csv"
	  	{ name: "adress",	
	  	  type: "relation",	// relational field 
	  	  domain: "town"}	// value is the ID on an observation in table "town.csv"	  	  
	  	]		
	  sequences:[ 
	  	{ name: "children"	 
	  	  type: "relation",	// relational field 
	  	  domain: "person"}	// values of this sequence are ID of observations in "person.csv"	  	  "
	  	] 	
	  patterns: [ ... ]		
	},
   company {...},			// dictionnary of company
   town {...},				// dictionnary of town
   ]

```

<div style="page-break-after: always;"></div>

--------
## Annex C : Representation  of hierarchical clustering results.

The output of hierarchical clustering is a binary tree whose nodes are corresponding to a cluster. Each cluster gathering either two others clusters, or one cluster and one observation, or two observations. Thus the output contain some relationnal information. Here, the idea is allow GDF to natively represent such kind of relational structure by using two dictionaries (see Annexe B):

- observations: the initial observations used to built the hierarchy
- clusters: the hierarchy, each nodes being define with either an observation or another cluster.

Example of declaration of such dataset.

```javascript
dictionary:[
   instances:{				// name of the file containing the instances
	  variables:[
	  	{ name: "firstname",
	  	  type: "string"},
	  	{ name: "employer",
	  	  type: "relation",	
	  	  domain: "company"},
	  	...
	  	],
	}
   clusters: {				// name of the file containing the clusters
	  variables:[ 
	  	{ name: "left-node"	 
	  	  type: "relation"
	  	  domain: ["instances", "clusters"]},	// left node: ID of an instance or a cluster
	  	{ name: "right-node"	 
	  	  type: "relation"
	  	  domain: ["instances", "clusters"]}	// right node: ID of an instance or a cluster
	  	...
	  	] 	
	},
   company {...},
   ]

```

<div style="page-break-after: always;"></div>

--------
## Annex D : Flat representation of the profile.

Using JSON format (or even a more simple one as YAML) it is clearly quite impossible to write the "profile" of a dataset with a text editor even if it is doted with a JSON mode. Thus we need to provide to the user a more simpler way to write it. There are two possibilities:

- the user can write a very first version using simple format (orange, weka) and then import the file. The main drawback of this approach being that most of GDF features will be ignored and a lot of work is required to complete the description of the data.
- providing another more user friendly external format for GDF. In practice, GDF format can be easily turn into a tabular format that can be edited with a very classical spreadsheet tool. The advantages of this approach are multiple: spreadsheet is a widespread tool, it is well-know by ton of users and, all in all, it is well-adapted to write semi-structured data. 

The target format to use for this file can be either :

- xlsx: native format of Microsoft Excel. Some [libraries exists](https://pypi.python.org/pypi/pyexcel-xls) in python to read/write this format.
- odf: native format of Apache Libre Office. Here too [libraries exists](https://pypi.python.org/pypi/pyexcel-ezodf) in python to read/write this format.

The structure of the flat external format is this one:

- The profile file is composed of several sheets:
	- a first sheet describing the header of the profile
	- a sheet per dictionnary and category of data among: variables, sequences and patterns. For instance, if the dictionary is named "data" we can have the three following sheets. Of course a sheet can de removed if there is no feature belonging to this category.
		- data.var : to define the variables (one line per variables)
		- data.seq : to define the sequences (one line per sequences)
		- data.pat : to define the patterns (one line per patterns)
	- optionally the file could also contains the table associated to each dictionary (for instance data.csv), in this way profile definition and main data are at the same place. Here there is two cases (see section 5):
		- GDF table: the files containing the sequence values must not appear here. 
		- DAF table: all data (including sequences) are stored in the table.

Furthermore with this organization it would be relativelly easy to provide some "template" containing macros allowing to simplify creation of the profile and to avoid some basic errors (wrong type name, ...)


<div style="page-break-after: always;"></div>

-------
## Annex E : Change log.

|Changes done with respect to version 0.40 | |
|Description |Sections concerned |
|-|-|
| A new meta-keyword *prototype* has been added to help coding of the clustering results. | 4.1|
| The  definition of variables and sequences has been modified. Now we have 3 properties: *type*, *domain* and *range* instead of 2. *Domain* expresses the way the type in represented and *range* the possible values or a domain restriction. | 4.1, 4.2 |
| The meta-keyword *weight* has been renamed *strength*. | 4.1|
| The meta-keyword *relevance* has been turned into a *property* in the dictionnary since it is more related to the semantic of the feature (is this information important or not) than just a meta-information.  | 4.1, 4.2, 4.3 |
| More precisions have been added concerning the meta-data for the sequences and the patterns. | 4.2, 4.3|
| The property *weight* has been added to the sequences to indicate when a sequence has an associated vector of weights. | 4.2|
| The property *inlined* has been added to the sequence allowing to directly store a sequence within a data table. | 4.2|
| Content of the annexes has been extended to provide further details about the possible improvements. | A, B, C, D |
| Correction of many typos as usual. | |

|Changes done with respect to version 0.32 | |
|Description |Sections concerned |
|-|-|
| Section 1 has been rewritten in order to provide a better outlook of the goals and interests of this work. | 1 |
| New section 2.3 discussing how to generalize the time series representation to deal with any kind of sequences. | 2.3 |
| Property "LSformatVersion" has been replaced by a more general "doctype" in the header. | 4 |
| Property "Authors" has been added to the header. | 4 |
| Major changes has been done in the list of types handled by the representation language. These modifications have two goal: first becoming closer to the classical types used in data analysis and second, to extend and generalize the representational possibilities. | 4.1, 4.2, 4.3|
| Notion of sequence has been generalized and now values can be indexed by any kind of ordered values and not just timestamps. | 4.2 |
| Correction of the definition of "vocabulary" in patterns description section which was not coherent with the way breakpoints are defined. | 4.3 |
| Property "variants" in patterns description has been renamed "indicators" which is more appropriate. | 4.3 |
| Property "length" in patterns description has been removed, this information is very dependant of the description language used to represent the patterns. If a tool is able to interpret this language it is also able to compute the size. | 4.3 |
| Several annexes has been added to describe some possible improvements to GDF. | A, ... |
| Correction of many typos as usual. | |
