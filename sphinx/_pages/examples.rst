Usage
=================

.. important::

    Some parameters are mandatatory, dont't forget them:
        * -s, ---source
        * -d, ---destination


.. important::

    If the **source format** is not given, it will use *'auto'* and try to **detect** the format.

.. important::

    **internal** ``GDF`` format will be the default export.

Simple
------

Using command line::

    lstranslator -s ./test/internal/BirdChicken/profile.json -f internal -d ./examples/ -t orange3

or it is also possible to call scipt from the repository::

    ./bin/lstranslator -s ./test/data/arff/car.arff -f arff -d ./examples/ -t internal

GDF
---

Modes how datatable will be stored
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Common json
***********

Using command line::

    lstranslator -s ./test/internal/example/profile.json -d ./examples/

.. note::

    It will creates :
        - a profile file ```profile.json``` containing header and descriptors.
        - a datatable as ```datataset_name.csv``` referencing other files (depending on the mode store sequence).
        - other files (sequences, patterns positions).

Spreadsheet without data
************************

Using command line::

    lstranslator -s ./test/internal/example/profile.json -d ./examples/ -md spreadhsheet_without_data

.. note::

    It will creates :
        - a spreadhseet file ```dataset_name.xlsx``` containing sheets ```header```, ```data.var```, ```data.seq```, ```data.pat```
        - a datatable file ```datataset_name.csv``` referencing other files (depending on the mode store sequence)
        - other files (sequences, patterns positions)

Spreadsheet with data
*********************

Using command line::

    lstranslator -s ./test/internal/examplexlsx/example.xlsx -f internal -d ./examples/ -md spreadhsheet_with_data

.. note::

    It will creates :
        - a spreadhseet file (dataset_name.xlsx) containing sheets ```header```, ```data.var```, ```data.seq```, ```data.pat```, ```data.gdf```. ```data.gdf```. sheet will be the datatable with cells referencing other files
        - other files (sequences, patterns positions).

Modes how the sequences will be stored
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

By sequences
************

Using command line::

    lstranslator -s ./test/internal/example/profile.json -d ./examples/ -ms **sequences**

.. note::

    It will creates a file by sequence descriptor and will add in datatable ```file.csv:line_id```

    In this file, each line is equal to a timseries. (referenced by the **line id**)

By observations
***************

Using command line::

    lstranslator -s ./test/internal/example/profile.json -d ./examples/ -ms **observations**

.. note::

    It will creates a file by option and will add in datatable ```obs1.csv```.

    In this this file, each column is equal to a timeseries. (referenced by a **column header**)

DAF
***

Using command line::

    lstranslator -s ./test/internal/example/profile.json -d ./examples/ -ms daf

Weka
----

Using command line::

    lstranslator -s ./test/internal/BirdChicken/profile.json -d ./examples/ -t arff

Ucr
---

Using command line::

    lstranslator -s ./test/internal/BirdChicken/profile.json -d ./examples/ -t ucr

Orange
------

Orange2
^^^^^^^

Using command line::

    lstranslator -s ./test/internal/BirdChicken/profile.json -d ./examples/ -t orange2

Orange3
^^^^^^^

Using command line::

    lstranslator -s ./test/internal/BirdChicken/profile.json -d ./examples/ -t orange3

Advanced
--------

Using command line::

    lstranslator -s ./test/internal/BirdChicken/profile.json -f internal -d ./examples/ -t orange3
