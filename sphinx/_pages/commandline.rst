Command ``lstranslator``
=========================

Synopsis
--------

**lstranslator** [*OPTIONS*] -s <*SOURCE_PATH*> -d <*OUTPUT_PATH*>

Description
-----------

| :program:`lstranslator` is a tool to translate a dataset from a format to another format.
| A transitional model is used and follow GDF specification.

*SOURCE_PATH* is the path to the source dataset, and *OUTPUT_PATH* is
the directory where the generated dataset is placed.

.. warning::

   ``lstranslator`` manages only dataset from format ``UCR``, ``WEKA``, ``ORANGE``, ``GDF``

Options
-------

.. program:: lstranslator

.. option:: -s, --source <SOURCE_PATH>

   Path where the source dataset will be found, throw an errow if not found

.. option:: -d, --destination <OUTPUT_PATH>

   Directory to place the output files. If it does not exist, it is created.

.. option:: -f, --from_format <FROM_FORMAT>

   Data Format from the source dataset (help to know which dataset to use).

   If this option is not chosen, detect FORMAT in looking on <SOURCE_PATH>

   Accepted formats:
    * ``UCR`` (.csv)
    * ``WEKA`` (.arff)
    * ``ORANGE2/ORANGE3`` (.csv, .tab)
    * ``RHM`` (_raw.json)
    * ``GDF`` (profile.json, dataset.xlsx)

.. option:: -t, --to_format <TO_FORMAT>

   Data Format wich the dataset will be translated. By default it uses ``GDF`` format.

   Possible formats:
    * ``UCR`` (.csv)
    * ``WEKA`` (.arff)
    * ``ORANGE2/ORANGE3`` (.csv, .tab)
    * ``GDF`` (profile.json, dataset.xlsx)

.. option:: -v, --verbosity

   Show all logs

.. option:: -a, --autogenerate

   Auto generate destination folder

.. option:: -md, ---mode_store_data <MODE_DATA>

   Mode how the data will be translate (used only in internal format)

   Possible modes:
    * ``spreadsheeet_with_data``
    * ``spreadsheeet_without_data``
    * ``json``

.. option:: -ms, ---mode_store_sequences <MODE_SEQUENCE>

   Mode how the sequences will be translate (used only in internal format)

   Possible modes:
    * ``auto``
    * ``observation``
    * ``sequence``
    * ``daf``