How to install
==============

Here is a **step by step** plan on how to install ``lstranslator``.
It will get you to a point of having a **local running instance**.


.. note::

    This package is still in development, it might have some errors during installation, don't forget to warn and share problems if there will be.

.. important::

    Requires ``git`` version >=2

.. important::

    Requires ``python`` >= 3.6

.. _Python 3.6: http://www.python.org/
.. _Git: http://git-scm.com/

Requirements
------------

You need **Python 3** or later work run ``lstranslator``.  You can have multiple Python
versions (2.x and 3.x) installed on the same system without problems.

In **Ubuntu, Mint and Debian** you can install Python 3 like this::

    sudo apt-get install python3 python3-pip

.. important::

    You will need administrator **rights**

For other Linux flavors, **OS X and Windows**, packages are available at

http://www.python.org/getit/

You will need to verify that your ``pip`` version is higher than 1.5 you can do this as such::

    pip --version

If this is not the case please update your pip version before continuing::

    pip install --upgrade pip

Using direct repository
-----------------------

If you don't have the repository, **clone** it (using ``git``)::

    git clone https://gricad-gitlab.univ-grenoble-alpes.fr/marinesj/ls_translator.git

.. important::

    This distant repository will only authorize you to access to it if you have the **rights**

Move inside the new folder::

    cd ./ls_translator

Install package::

    python setup.py install

Using ``pip``
-------------

Use the ``pip`` command to install it from the **distant repository**::

    pip3 install git+https://gricad-gitlab.univ-grenoble-alpes.fr/marinesj/ls_translator.git

.. important::

    This distant repository will only authorize you to access to it if you have the **rights**

Use the ``pip`` command to install it from **local source**::

    pip3 install

Dependencies
------------

* numpy
    use to calculate and play with array and mathematics
* liac_arff
    read and play with *.arff* file
* argcomplete
    autocompleter
* openpyxl
    read and play with *.xlsx* file

.. note::

    Dependencies will be automatically installed in same time as the ``lstranslator`` (**setup.py**) installation will processed

Testing
-------

.. note::

    ``lstranslator`` command should be available, to use it see ..

Install pytest (if it's not already done) ::

    pip3 install pytest

Run test by ``pytest`` command::

    pytest

Run test using script::

    python testsuite.py # run main instruction

All elements should be ok.