# coding=UTF-8

"""ArffParser module."""

import os
import arff

from lstranslator.models.learningset import LearningSet
from lstranslator.models.observation import Observation
from lstranslator.enum.gdftype import GDFType
from lstranslator.enum.domain import Domain
from lstranslator.models.column import VariableColumn

from lstranslator.parser.interface import IParser


def _get_value(variable_name):
    """Get lambda expression to get value from a specific variable name

    Arguments:
        variable_name {str} -- variable name

    Returns:
        lambda -- lambda expression
    """

    return lambda o: o.get(variable_name)

class ArffParser(IParser):
    """Class to parse a WEKA dataset (arff) from a Learningset.

    Implements IParser interface.
    """

    def __init__(self):
        """Constructor
        """

        self._obs_prefix = 'obs_'
        self._delimiter = '\t'
        self._extension_file = '.arff'

    def parse_from_path(self, input_file):
        """Parse a file with an **.arff** extension, from a filepath

        Arguments:
            input_file {str} -- filepath

        Raises:
            FileNotFoundError -- File doesnt exist
            FileNotFoundError -- No such with the given filename
            IOError -- Error in the csv reader
            ValueError -- Number of listed columns and number of listed variables is not equal

        Returns:
            LearningSet -- learningset
        """

        if not os.path.exists(input_file):
            raise FileNotFoundError("File doesnt exist ({})".format(input_file))

        if not self._extension_file in input_file:
            raise FileNotFoundError("No such file with filename({})" \
                          .format(self._extension_file))

        ls_name = os.path.basename(input_file).split(self._extension_file)[0]
        observations = list()
        labels = list()
        variables = list()

        variables.append(VariableColumn('@id', v_type=GDFType.STRING, domain=Domain.NONE,
                                  metas=[['fake'], ['id']]))

        with open(input_file, 'r') as file:
            file_data = arff.load(file)
            # keep description in the ls
            description = file_data.get('description')
            # use relation value as ls name
            ls_name = file_data.get('relation', ls_name)

            # loop attributes to compute variables
            for attribute in file_data.get('attributes'):

                _metas = []
                _name = attribute[0]
                _range = []
                _value_type = GDFType.NUMBER
                _v_domain = Domain.NONE
                if attribute[1] in ['REAL', 'real']:
                    _v_domain = Domain.FLOAT
                elif attribute[1] in ['INTEGER', 'integer']:
                    _v_domain = Domain.INTEGER
                elif attribute[1] in ['NUMERIC', 'numeric']:
                    _v_domain = Domain.FLOAT
                elif attribute[1] in ['STRING', 'string']:
                    _value_type = GDFType.STRING
                elif isinstance(attribute[1], str):
                    # type is not managed in the switchcase
                    raise IOError('not compliant datatype {}'.format(attribute[1]))
                else:
                    _value_type = GDFType.ENUMERATE
                    _v_domain = Domain.STRING
                    _range = attribute[1]

                if str(attribute[0]).lower() == 'class':
                    _metas.append(['class'])
                    # use _domain to find labels
                    labels = _range

                variables.append(VariableColumn(_name, domain=_v_domain, v_type=_value_type, v_range=_range, metas=_metas))

        _observations = file_data.get('data')
        nb_digit = len(str(len(_observations)))

        # construct observations
        for line_id, observation in enumerate(_observations):
            _obs_id = self._obs_prefix + str(line_id).zfill(nb_digit)
            _obs_data = [_obs_id] + observation
            if len(_obs_data) != len(variables):
                raise ValueError('number of variables and number of values is not equal {} vs {} - {}'\
                                 .format(len(_obs_data), len(variables), variables))

            observations.append(Observation(_obs_id, variables, _obs_data))

        # min / max for continuous variable
        for i, variable in enumerate(variables):
            if variable.type == GDFType.NUMBER:
                _values = [obs.data[i] for obs in observations]
                variable.range = [min(_values), max(_values)]

        return LearningSet(observations=observations, timeseries=list(),
                           labels=labels, variables=variables, name=ls_name,
                           features=list(), patterns=list(), description=description)
