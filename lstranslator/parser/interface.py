# coding=UTF-8

"""
    Parser Interface
"""

import abc

class IParser(abc.ABC):
    """ Class used as an interface """

    @abc.abstractmethod
    def parse_from_path(self):
        """ Abstract method of parse_from_path """
        pass
