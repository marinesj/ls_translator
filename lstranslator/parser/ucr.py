# coding=UTF-8

"""This module manages UCR parsing."""

import os
import numpy as np

from lstranslator.models.learningset import LearningSet
from lstranslator.models.observation import Observation
from lstranslator.enum.variabletype import VariableType as Type
from lstranslator.enum.gdftype import GDFType
from lstranslator.enum.domain import Domain
from lstranslator.models.timeseries import TimeSeries

from lstranslator.parser.interface import IParser
from lstranslator.models.column import VariableColumn, SequenceColumn, PatternColumn

class UcrParser(IParser):
    """This class allows parsing learning set as UCR format."""

    def __init__(self):
        self._obs_prefix = 'obs_'
        self._ts_prefix = 'TS_'
        self._delimiter = ','

    def parse_from_path(self, input_file, delimiter=',', ls_name=None):
        """ Parse ucr dataset

        Arguments:
            input_file {[type]} -- [description]

        Keyword Arguments:
            delimiter {string} -- [description] (default: {','})
            ls_name {string} -- [description] (default: {None})

        Raises:
            FileNotFoundError -- if not file or directory

        Returns:
            LearningSet -- parsed learning set
        """

        if not os.path.exists(input_file):
            raise FileNotFoundError("No such file or directory ({})".format(input_file))
        if not ls_name:
            ls_name = os.path.basename(input_file).split('.')[0]

        variables = [VariableColumn('@id', domain=Domain.STRING, v_type=GDFType.ENUMERATE, v_range=None, units='_', comment="fake id",
                              metas=[['fake'], ['id']]),
                     SequenceColumn(ls_name + '_variable', domain=Domain.FLOAT, v_type=GDFType.NUMBER, v_range=(-5, 5), index="integer", sampling="uneven"),
                     VariableColumn('class', v_type=GDFType.ENUMERATE, domain=Domain.STRING, v_range=[], units='_', comment="label", metas=[['class']])]
        observations = []
        timeseries = []
        labels = []

        with open(input_file, 'r') as _file:
            lines = _file.readlines()
            line_id = 0
            nb_digit = len(str(len(lines)))
            for line in lines:
                _arr = line.split(delimiter)
                if _arr[0] not in labels:
                    labels.append(_arr[0])
                obs_data = []
                if len(_arr) > 1:
                    obs_id = self._obs_prefix + str(line_id).zfill(nb_digit)
                    obs_data.append(obs_id)
                    ts_name = self._ts_prefix + str(line_id).zfill(nb_digit)
                    ts_data = TimeSeries(np.array([x for x in _arr[1:]],
                                                  dtype=np.float64), ts_name, [], obs_id, ts_name)
                    obs_data.append(ts_data)
                    obs_data.append(_arr[0])

                    observations.append(Observation(obs_id=obs_id, data=obs_data, variables=variables))
                    timeseries.append(ts_data)
                line_id += 1
            # add labels domain

            func = lambda o: np.amax(o.data[int(index)].data)
            for index, variable in enumerate(variables):
                if variable.type == GDFType.NUMBER:
                    func_min = lambda o: np.amax(o.data[int(index)].data)
                    func_max = lambda o: np.amin(o.data[int(index)].data)
                    min_value = min(map(func_min, observations))
                    max_value = max(map(func_max, observations))
                    variable.range = (min_value, max_value)
                elif variable.type == GDFType.ENUMERATE:
                    func = lambda o: o.data[int(index)]
                    variable.range = np.unique(list(map(func, observations))).tolist()

        return LearningSet(name=ls_name,
                           observations=observations,
                           variables=variables,
                           timeseries=timeseries,
                           labels=labels,
                           features=[])
