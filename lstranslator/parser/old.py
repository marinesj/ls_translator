# coding=UTF-8

"""This module manages parsing old format used by hmi."""

import json
from os import path as ospath

from lstranslator.parser.interface import IParser

from lstranslator.models.column import VariableColumn, PatternColumn, SequenceColumn
from lstranslator.enum.variabletype import VariableType
from lstranslator.enum.domain import Domain
from lstranslator.enum.gdftype import GDFType
from lstranslator.models.observation import Observation
from lstranslator.models.learningset import LearningSet
from lstranslator.models.timeseries import TimeSeries
from lstranslator.models.pattern import Pattern

class OldParser(IParser):
    """
        Old class parser
    """
    def __init__(self):
        self._raw_extension = '_raw.json'
        self._disc_extension = '_disc.json'
        self._pattern_extension = '_pattern.json'
        self._obs_prefix = 'obs_'
        self._ts_prefix = 'TS_'

    def parse_from_path(self, input_file):
        """
            main parse methode
        """
        # construct filepath
        if not ospath.isfile(input_file):
            raise FileNotFoundError("No such file ({})".format(input_file))

        if self._raw_extension not in input_file:
            raise FileNotFoundError("Filename should finish ({}), but path is {}" \
                        .format(self._raw_extension, input_file))

        ls_name = ospath.basename(input_file).split(self._raw_extension)[0]
        dirname = ospath.dirname(input_file)

        variables = []
        raw_variables = []
        labels = []
        timeseries = []
        patterns = []

        patterns_data = None
        with open(ospath.join(dirname, ls_name + self._pattern_extension)) as file:
            patterns_data = json.load(file)

        discretized_data = None
        with open(ospath.join(dirname, ls_name + self._disc_extension)) as file:
            discretized_data = json.load(file)

        raw_data = None
        with open(ospath.join(dirname, ls_name + self._raw_extension)) as file:
            raw_data = json.load(file)

        raw_meta_data = raw_data.get('meta_data', {})
        disc_meta_data = discretized_data.get('meta_data', {})
        variable_names = [v for v in raw_meta_data.get('variables', []) if '-' not in v]

        bps = { name: [float(x) for x in raw_meta_data.get('break_points', [])[i]] for i, name in enumerate(variable_names) }
        disc = { name: disc_meta_data.get('break_points', [])[i] for i, name in enumerate(variable_names)}

        n = 0
        for pattern_data in patterns_data.values():
            variables_expanded = pattern_data.get('variable', '').split('-')
            bps_tmp = [bps.get(v) for v in variables_expanded]
            disc_tmp = [disc.get(v) for v in variables_expanded]
            pattern = Pattern(name='pattern{}'.format(n), variables=variables_expanded, break_points=bps_tmp,
                              interpreter=['RHM'],
                              length=pattern_data.get('length'), variants=['exist', 'count', 'pos'], vocabulary=disc_tmp,
                              regex=pattern_data.get('regex'), locations=pattern_data.get('locations'))
            patterns.append(pattern)
            n += 1

        min_y = raw_meta_data.get('min_y', [])
        max_y = raw_meta_data.get('max_y', [])
        observations = []

        variables.append(VariableColumn('obs_id', v_type=GDFType.STRING, domain=Domain.NONE, metas=[['id']]))
        for i, variable_name in enumerate(variable_names):
            raw_variables.append(SequenceColumn(variable_name, domain=Domain.FLOAT, v_type=GDFType.NUMBER, index="integer", sampling="uneven", v_range=(min_y[i], max_y[i])))
        variables = variables + raw_variables
        for pattern in patterns:
            v_exist = PatternColumn(pattern.name + '/exist', domain=Domain.NONE, v_type=GDFType.BOOLEAN, definition=pattern.name, sequences=pattern.variables, generator=['RHM'], comment=pattern)
            v_count = PatternColumn(pattern.name + '/count', domain=Domain.INTEGER, v_type=GDFType.NUMBER, definition=pattern.name, sequences=pattern.variables, generator=['RHM'], comment=pattern)
            v_pos = PatternColumn(pattern.name + '/pos', domain=Domain.INTEGER, v_type=GDFType.NUMBER, definition=pattern.name, sequences=pattern.variables, generator=['RHM'], comment=pattern)
            variables += [v_exist, v_count, v_pos]
        variables.append(VariableColumn('class', v_type=GDFType.ENUMERATE, domain=Domain.STRING, metas=[['class']]))

        _timeseries = raw_data.get('raw', {})
        for obs_id, timeserie in _timeseries.items():
            label = str(timeserie.get('class', ''))
            if label and label not in labels:
                labels.append(label)
            data = timeserie.get('data')
            obs_ts = []
            obs_data = []
            index = 0
            for i, variable in enumerate(variables):
                if variable.has_meta('id'):
                    obs_data.append(obs_id)
                elif variable.has_meta('class'):
                    obs_data.append(label)
                elif variable.imp_type == VariableType.PATTERN:
                    locations = variable.description.locations.get(obs_id, [])
                    variant = variable.name.split('/')[1]
                    data = None
                    if variant == 'pos':
                        data = locations
                    elif variant == 'exist':
                        data = len(locations) > 0
                    elif variant == 'count':
                        data = len(locations)
                    obs_data.append(data)
                elif variable.imp_type == VariableType.TIMESERIES:
                    ts_name = self._ts_prefix + obs_id + "_" + variable.name
                    sequence = TimeSeries(data[index], ts_name, [], obs_id, variable.name)
                    index += 1
                    obs_ts.append(sequence)
                    obs_data.append(sequence)
            timeseries += obs_ts

#            obs_data.append(label)
            observations.append(Observation(obs_id, variables, obs_data))
        variables[-1].range = labels
        return LearningSet(observations=observations, timeseries=timeseries,
                           labels=labels, variables=variables, name=ls_name, features=[], patterns=patterns)
