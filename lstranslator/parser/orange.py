# coding=UTF-8

"""This module manages ``ORANGE`` parsing.

**Orange2** format ::

    - in the first line, we have names
    - in the second line, the types
    - in the third line, the features

    - c or continuous for features that are continuous,
    - d or discrete for features that are discrete,
    - t or time for features that represent date and/or time in one of the ISO 8601 formats,
    - s or string for string features.


**Orange3** format ::

    in the first line, # should be used to compute format 'S#toto'

    c for class feature,
    i for feature to be ignored,
    m for meta attributes (not used in learning),
    C for features that are continuous,
    D for features that are discrete,
    T for features that represent date and/or time in one of the ISO 8601 formats,
    S for string features.

"""

import csv

from os import path as ospath
from lstranslator.enum.variabletype import VariableType
from lstranslator.models.observation import Observation
from lstranslator.models.learningset import LearningSet
from lstranslator.parser.interface import IParser
from lstranslator.enum.domain import Domain
from lstranslator.enum.gdftype import GDFType
from lstranslator.models.column import VariableColumn

class OrangeParser(IParser):
    """
        This class is able to parse ``ORANGE`` dataset as a Learningset.
    """

    def __init__(self):
        self._obs_prefix = 'obs_'
        self._ts_prefix = 'TS_'

    def _parse_orange2_header(self, header, variable_types, variable_metadatas):
        all_variables = []
        for index, column in enumerate(header):
            orange_type = variable_types[index]
            value_type = None
            variable_type = None
            metas = []
            if variable_metadatas[index] in ['class', 'c']:
                metas = [['class']]

            if orange_type in ['c', 'continuous']:
                variable_type = GDFType.NUMBER
                value_type = Domain.FLOAT
            elif orange_type in ['s', 'string']:
                variable_type = GDFType.STRING
                value_type = Domain.NONE
            elif orange_type in ['t', 'time']:
                variable_type = VariableType.TIMESTAMP
                value_type = Domain.TIMESTAMP
            elif orange_type in ['d', 'discrete']:
                variable_type = GDFType.ENUMERATE
                value_type = Domain.STRING

            variable = VariableColumn(column, domain=value_type, v_type=variable_type, metas=metas)
            all_variables.append(variable)
        return all_variables

    def _parse_orange3_header(self, header):
        all_variables = []
        for column in header:
            attributes, name = column.split('#')
            value_type = None
            variable_type = None

            metas = []
            if 'c' in attributes:
                metas = [['class']]

            if 'm' in attributes:
                pass
            if 'i' in attributes:
                pass

            if 'C' in attributes:
                variable_type = GDFType.NUMBER
                value_type = Domain.FLOAT
            elif 'S' in attributes:
                variable_type = GDFType.STRING
                value_type = Domain.NONE
            elif 'T' in attributes:
                variable_type = GDFType.TIMESTAMP
                value_type = Domain.TIMESTAMP
            elif 'D' in attributes:
                variable_type = GDFType.ENUMERATE
                value_type = Domain.STRING

            variable = VariableColumn(name=name, domain=value_type, v_type=variable_type, metas=metas)
            all_variables.append(variable)
        return all_variables

    def parse_from_path(self, input_file, delimiter='\t', ls_name=None):
        """ Parse an orange dataset, can be in v2 or v3

        Arguments:
            input_file {string} -- [description]

        Keyword Arguments:
            ls_name {string} -- learningset name (default: {None})

        Raises:
            FileNotFoundError -- If file is not found

        Returns:
            LearningSet -- parsed learningset
        """

        # construct filepath
        if not ospath.isfile(input_file):
            raise FileNotFoundError("No such file ({})".format(input_file))

        if ls_name is None:
            ls_name = ospath.basename(input_file).split('.')[0]

        with open(input_file, newline='') as csvfile:
            csv_reader = csv.reader(csvfile, delimiter=delimiter, quotechar='"',
                                    quoting=csv.QUOTE_ALL) # read csv

            variable_names = next(csv_reader) # first line (headers)
            variables = [VariableColumn('@id', v_type=GDFType.STRING, domain=Domain.NONE, v_range=None, metas=[['id'], ['fake']])] # convert headers to VariableColumn

            # orange 3 parsing
            is_orange3 = len([x for x in variable_names if len(x.split('#')) > 1]) > 0
            if is_orange3:
                variables += self._parse_orange3_header(variable_names)

            # orange 2 parsing
            else:
                variable_types = next(csv_reader)
                variable_metadatas = next(csv_reader)
                variables += self._parse_orange2_header(variable_names,
                                                            variable_types,
                                                            variable_metadatas)

            labels = []
            liste = list(csv_reader)
            nb_digit = len(str(len(liste)))

            observations = []
            for line_id, row in enumerate(liste):
                label = ''
                data = []

                obs_id = self._obs_prefix + str(line_id).zfill(nb_digit)
                row = [obs_id] + row
                for index, variable in enumerate(variables):
                    value = row[index]
                    if variable.domain == Domain.FLOAT:
                        value = float(value)
                    data.append(value)

                    if variable.has_meta('class'):
                        label = row[index]

                if label not in labels:
                    labels.append(label)

                observations.append(Observation(obs_id=obs_id, data=data, variables=variables))

            func = lambda o: o.data[index]

            # compute range
            for variable in variables:
                if variable.domain is Domain.FLOAT:
                    min_value = min(map(func, observations))
                    max_value = max(map(func, observations))
                    variable.range = (min_value, max_value)

            return LearningSet(name=ls_name,
                               observations=observations,
                               variables=variables,
                               timeseries={},
                               labels=labels,
                               features=[])
