# coding=UTF-8

"""This class does blah blah."""

import os, json, csv, sys

from ast import literal_eval
from openpyxl import load_workbook
from collections import OrderedDict

from lstranslator.models.learningset import LearningSet
from lstranslator.models.observation import Observation
from lstranslator.enum.variabletype import VariableType
from lstranslator.enum.gdftype import GDFType

from lstranslator.models.timeseries import TimeSeries
from lstranslator.models.pattern import Pattern
from lstranslator.enum.domain import Domain

from lstranslator.parser.interface import IParser

from lstranslator.helper.logger import logger
from lstranslator.models.column import VariableColumn, SequenceColumn, PatternColumn

from lstranslator.config import ALLOWED_CSV_DELIMITERS, LINE_SEPARATORS, DATA_DAF_SHEET_KEY, \
                                DATA_GDF_SHEET_KEY, PATTERN_SHEET_KEY, SEQUENCE_SHEET_KEY, VARIABLE_SHEET_KEY

def _is_not_empty(value):
    """
        Check if the value is not empty
        :param value
        :return:boolean
    """
    return value is not None and value != ""

def _index_of_first(lst, pred):
    """
        Get the first index where the condition is True
        :param lst:list
        :param pred: function
        :return:integer
    """
    for i,v in enumerate(lst):
        if pred(v):
            return i
    return None

def _read_csv(data_path):
    """
        Read csv file
        :param data_path:string path to get the csv file
        :return:list
    """
    if not os.path.exists(data_path):
        raise FileNotFoundError("File doesnt exist ({})".format(data_path))

    with open(data_path) as csvfile:
        # detect delimiter
        line = csvfile.read(1024)
        csvfile.seek(0)
        delimiter = ','
        nb = 0
        for d in ALLOWED_CSV_DELIMITERS: #loop possible delimiters
            tmp = line.count(d)
            if tmp > nb:
                nb = tmp
                delimiter = d
        #read csv
        return list(csv.reader(csvfile, delimiter=delimiter))

def _is_digit(value):
    """
        Quick helper to know if the value is a number (float or int)
        :param value:string value to check
        :return:boolean
    """
    return str(value).replace('.', '', 1).replace('-', '').isdigit()

def _convert_variables(variables_array, type, patterns_dict={}):
    """
        Convert raw variables to gdf variables
        :param variables_array:list raw variables
        :param is_ts:boolean
        :param patterns_dict:dict pattern that could be linked to a variable
        :return:list
    """
    results = []
    for variable in variables_array:
        v_name = variable.get('name')
        for name in variable.get('names', [v_name] if not isinstance(v_name, list) else v_name):
            if type == VariableType.PATTERN:
                for variant_name in variable.get('variants', variable.get('indicators', [])):
                    if str(variant_name).lower() == 'exist':
                        value_type = GDFType.BOOLEAN
                        domain_type = Domain.NONE
                    else:
                        value_type = GDFType.NUMBER
                        domain_type = Domain.INTEGER
                    var = PatternColumn(name=name + '/' + variant_name,
                                   v_type=value_type,
                                   domain=domain_type,
                                   definition=variable.get('definition'),
                                   generator = variable.get('generator'),
                                   sequences=variable.get('sequences', []),
                                   breakpoints=variable.get('breakpoints'),
                                   indicator=variant_name,
                                   vocabulary=variable.get('vocabulary'),
                                   comment=patterns_dict.get(name),
                                   metas=variable.get('meta', []))
                    results.append(var)
            else:
                value_type = GDFType[str(variable.get('type')).upper()]
                domain_type = Domain[str(variable.get('domain')).upper()]
                v_range = variable.get('range')
                if v_range and value_type == GDFType.NUMBER:
                    v_range = [int(v) if (domain_type == Domain.INTEGER) else float(v) for v in v_range]
                if type == VariableType.TIMESERIES:
                    var = SequenceColumn(name=name,
                                         v_type=value_type,
                                         domain=domain_type,
                                         index=variable.get('index'),
                                         sampling=variable.get('sampling'),
                                         v_range=v_range,
                                         relevance=variable.get('relevance'),
                                         weighted=variable.get('weighted'),
                                         metas=variable.get('meta', []),
                                         comment=variable.get('comment'),
                                         units=variable.get('units'))
                else:
                    var = VariableColumn(name=name,
                                         v_type=value_type,
                                         domain=domain_type,
                                         v_range=v_range,
                                         relevance=variable.get('relevance'),
                                         metas=variable.get('meta', []),
                                         comment=variable.get('comment'),
                                         units=variable.get('units'))
                results.append(var)
    return results

class InternalParser(IParser):
    """
        Class to manage parsing from internal data format
    """

    def __init__(self):
        # use to compute observation identifier
        self._obs_prefix = 'obs_'
        # use to compute timeseries identifier
        self._ts_prefix = 'TS_'
        # extension of the output file
        self._extension_file = '.csv'
        # filename of the main profile filename
        self._profile_filename = 'profile.json'
        # files that are already managed (pattern_pos or ts values)
        self._files = {}

    def _build_learningset(self, columns, rows, dirname, ls_name_raw):
        """Build learning set

        Arguments:
            columns {list} -- [description]
            rows {list} -- [description]
            dirname {str} -- [description]
            ls_name_raw {str} -- [description]

        Returns:
            LearningSet -- [description]
        """

        classic_variables = columns.get('variables', [])
        sequence_variables = columns.get('sequence', columns.get('sequences',[]))
        pattern_variables = columns.get('patterns', [])

        all_patterns_dict = OrderedDict()

        # generate patterns in a dict
        for pv in pattern_variables:
            name = pv.get('name', [])
            for pv_name in pv.get('names', name if isinstance(name, list) else [name]):
                pattern = Pattern(name=pv_name, regex=pv.get('definition'),
                                    variables=pv.get('sequences'), break_points=pv.get('breakpoints'),
                                    length=None, interpreter=pv.get('generator'),
                                    variants=pv.get('variants', pv.get('indicators', [])), locations={},
                                    vocabulary=pv.get('vocabulary'),
                                    slice_type=pv.get('slice'), sampling=pv.get('sampling'))
                all_patterns_dict[pattern.name] = pattern
        logger.debug('patterns dict : {}'.format(all_patterns_dict))
        # generate variables as an array
        all_variables = _convert_variables(classic_variables, VariableType.VARIABLE)
        all_variables += _convert_variables(sequence_variables, VariableType.TIMESERIES)
        all_variables += _convert_variables(pattern_variables, VariableType.PATTERN, patterns_dict=all_patterns_dict)

        patterns_data = {}

        observations, timeseries, labels = self._parse_data_table(rows, all_variables, dirname, patterns_data)
        self._files = {}

        # loop on patterns
        for pattern_name, p_data in patterns_data.items():
            # set locations
            all_patterns_dict[pattern_name].locations = p_data.get('pos')

        return LearningSet(observations=observations, timeseries=timeseries,
                          labels=labels, variables=all_variables, name=ls_name_raw, features={}, patterns=list(all_patterns_dict.values()))

    def _parse_observation(self, variables, data, line_id, obs_id, dirname, patterns_data, timeseries, labels):
        """Parse line in looking on variables to generate an Observation

        Arguments:
            variables {list} -- list of variables
            data {list} -- data with the values
            line_id {int} -- index to look inside csv file
            obs_id {str} -- observation identifier
            dirname {str} -- folder where the learning set will be created
            patterns_data {dict} -- patterns as dict (shared)
            timeseries {dict} -- timeseries as dict (shared)
            labels {list} -- list of labels (shared)

        Returns:
            Observation -- object
        """

        obs_label = None
        obs_data = []
        for j, variable in enumerate(variables):
            v_data = data[j]

            if isinstance(v_data, list) and len(v_data) == 1:
                v_data = v_data[0]

            if isinstance(v_data, str):
                v_data = v_data.strip()
            # meta managements
            if variable.has_meta('class'):
                obs_label = v_data

            # specific format
            # CONTINUOUS
            if variable.imp_type is VariableType.VARIABLE:
                if variable.domain is Domain.FLOAT:
                    # format as float
                    v_data = float(v_data)
                elif variable.domain is Domain.INTEGER:
                    v_data = int(v_data)
            # PATTERN
            elif variable.imp_type is VariableType.PATTERN:
                pattern_name, pattern_variant = variable.name.split('/')
                # use literal_eval to detect and format data value
                if pattern_variant == 'pos':
                    if isinstance(v_data, str):
                        v_data = [literal_eval(v) for v in self._get_linked_data(v_data, dirname, line_id, variable)]
                    else:
                        v_data = list(v_data)
                elif isinstance(v_data, str) and variable.type is not GDFType.STRING:
                    if (variable.type is GDFType.BOOLEAN):
                        v_data = v_data in ['True', True, 1, '1']
                    elif (variable.domain is Domain.FLOAT):
                        v_data = float(v_data)
                    elif (variable.domain is Domain.INTEGER):
                        v_data = int(v_data)
                    else:
                        v_data = literal_eval(v_data)
                pattern_data = patterns_data.setdefault(pattern_name, {})
                pattern_variant_data = pattern_data.setdefault(pattern_variant, {})
                pattern_variant_data[obs_id] = v_data
            # TIMESERIES
            elif variable.imp_type is VariableType.TIMESERIES:
                # keep sequence content
                if isinstance(v_data, str):
                    ts_data = self._get_linked_data(v_data, dirname, line_id, variable)
                else:
                    ts_data = list(v_data)
                ts_name = self._ts_prefix + obs_id + "_" + variable.name
                # compute timeseries
                v_data = TimeSeries(ts_data, ts_name,
                                    [], obs_id, variable.name)
                timeseries.append(v_data)
            obs_data.append(v_data)
        # keep class in labels
        if obs_label and obs_label not in labels:
            labels.append(obs_label)

        return Observation(obs_id, variables, obs_data)

    def _parse_data_table(self, ls_data, all_variables, dirname, patterns_data):
        """
            Parse main datatable
            :param ls_data:list
            :param all_variables:list
            :param dirname:string
            :param patterns_data:dict
            :return:list
        """
        all_variables_dict = {v.name:v for v in all_variables}
        ls_names = [c.strip().replace('\\', '') for c in ls_data[0]]
        observations = []
        timeseries = []
        labels = []

        variables = [all_variables_dict.get(variable_name) for variable_name in ls_names]

        if len(ls_names) != len(all_variables):
            logger.error('number of columns and variables (profile.json) is not equal ({} vs {})'.format(len(ls_names), len(all_variables)))

        # list variables in following ordering from main file
        variables = [all_variables_dict[var] for var in ls_names]

        nb_digit = len(str(len(ls_data[1:])))

        obs_data = []
        obs_id = None
        incr = 0
        line_cache_id = None

        for line_id, line in enumerate(ls_data[1:]):
            tmp_id = None
            # get id from line in looking on variables
            for j, variable in enumerate(variables):
                if variable.has_meta('id'):
                    value = line[j]
                    if not value:
                        tmp_id = obs_id
                    else:
                        tmp_id = str(value).strip()

            # if there is no idea
            if not tmp_id and not obs_id:
                line_cache_id = line_id
                tmp_id = self._obs_prefix + str(incr).zfill(nb_digit)
                logger.debug('autogenerate obs id {}'.format(tmp_id))
                incr += 1

            if obs_id != tmp_id:
                if obs_id is not None:
                    observations.append(self._parse_observation(variables, obs_data, line_cache_id, obs_id, dirname, patterns_data, timeseries, labels))
                line_cache_id = line_id
                    # finalize obs
                obs_data = line
                if tmp_id:
                    obs_id = tmp_id
                else:
                    obs_id = self._obs_prefix + str(incr).zfill(nb_digit)
                    incr += 1
                    logger.debug('autogenerate obs id {}'.format(obs_id))
            else:
                for j in range(len((ls_names))):
                    if line[j]:
                        if not isinstance(obs_data[j], list):
                            obs_data[j] = [obs_data[j]]
                        obs_data[j].append(line[j])
        if obs_id:
            observations.append(self._parse_observation(variables, obs_data, line_cache_id, obs_id, dirname, patterns_data, timeseries, labels))

        return (observations, timeseries, labels)

    def _parse_sheet(self, sheet):
        """
            Convert sheet to a simple array
            :param sheet
            :return:list
        """
        result = []
        headers = {}
        for row in sheet.rows:
            if headers == {}:
                cells = [c.value.strip() for c in row if c.value is not None]
                if (len(cells) > 5):
                    headers = cells
            else:
                line = {}
                for j, cell in enumerate(row):
                    value = cell.value
                    if cell.data_type is "s":
                        value = value.strip()
                        if ',' in value:
                            value = [v.strip() for v in value.split(',')]
                    if headers[j] in ['meta', 'length', 'names', 'indicators', 'variants', 'generator']:
                        if value is None:
                            value = []
                        elif not isinstance(value, list):
                            value = [value]
                            if (headers[j] == 'meta'):
                                value = [value] # FIXME: hacky
                    if value is not None:
                        line[headers[j]] = value
                result.append(line)
        return result

    def _get_linked_data(self, v_data, dirname, lineid, variable):
        """
            :param v_data
            :param dirname:string
            :param lineid:integer
            :param variable:Column
            :return
        """
        line_id = None
        v_filename = v_data
        for d in LINE_SEPARATORS:
            if d in v_data:
                v_filename, line_id = v_data.split(d)

        if not self._files.get(v_filename):
            self._files[v_filename] = _read_csv(os.path.join(dirname, v_filename)) # cache data

        data = self._files[v_filename]

        if data[0] and data[0][0] and not _is_digit(data[0][0]): # means columns strategy
            col_idx = data[0].index(variable.name) # get idx to get the variable value
            values = [d[col_idx] for d in data[1:]] # prepare values
            idx_not_empty = _index_of_first(values[::-1], _is_not_empty) or 0 # get first idx with not empty value from reversed values
            return values[:-idx_not_empty] if idx_not_empty else values # get data

        if not line_id:
            if len(data) == 1:
                line_id = 0 # means vector is the file
            else:
                line_id = lineid # use index from obs
        elif isinstance(line_id, str):
            line_id = int(line_id) #

        return data[line_id]

    def _parse_from_json(self, input_file):
        """
            Parse GDF LS, as json mode (normal one)
            :param input_file :string path to get the LS
            :return learningset
        """
        if self._profile_filename not in input_file:
            raise FileNotFoundError("No such file with filename({})" \
                                    .format(self._profile_filename))

        with open(input_file) as profile_file:
            profile_data = json.load(profile_file)

        if not profile_data:
            raise ValueError("Profile data doesnt exist")

        profile_header = profile_data.get('header')
        dirname = os.path.dirname(input_file)

        if not profile_header:
            raise ValueError("Header from profile doesnt exist")
        ls_name_raw = profile_header.get('dataset')

        features = profile_data.get('features', profile_data.get('dictionary'))

        ls_data = _read_csv(os.path.join(dirname, ls_name_raw + self._extension_file))
        return self._build_learningset(features, ls_data, dirname, ls_name_raw)

    def _parse_from_xlsx(self, input_file):
        """
            Parse GDF LS, as spreadsheet mode
            :param input_file :string path to get the LS
            :return learningset
        """
        if "xlsx" not in input_file:
            raise FileNotFoundError("No such file with filename({})" \
                                    .format("xlsx"))

        wb = load_workbook(filename=input_file, read_only=True)

        header_sheet = wb['header']
        header = {}
        for row in header_sheet.rows:
            key = None
            for j, cell in enumerate(row):
                if cell.value:
                    if j is 0:
                        key = cell.value
                        header[key] = []
                    else:
                        header[key].append(cell.value)

        features = {
            "patterns": self._parse_sheet(wb[PATTERN_SHEET_KEY]),
            "sequences": self._parse_sheet(wb[SEQUENCE_SHEET_KEY]),
            "variables": self._parse_sheet(wb[VARIABLE_SHEET_KEY])
        }

        logger.debug('found features : {}'.format(features))

        if (DATA_GDF_SHEET_KEY in wb.sheetnames):
            rows = [[c.value for c in r] for r in list(wb[DATA_GDF_SHEET_KEY].rows)]
        elif ("data.csv" in wb.sheetnames):
            rows = [[c.value for c in r] for r in list(wb["data.csv"].rows)]
        elif DATA_DAF_SHEET_KEY in wb.sheetnames:
            rows = [[c.value for c in r] for r in list(wb[DATA_DAF_SHEET_KEY].rows)]
        else:
            rows = _read_csv(os.path.join(os.path.dirname(input_file), header.get('dataset')[0] + self._extension_file))

        return self._build_learningset(features, rows, os.path.dirname(input_file), header.get('dataset')[0])

    def parse_from_path(self, input_file):
        """
            Parse input_file
            :param input_file: internal format
            :return: LearningSet
        """

        if not os.path.exists(input_file):
            raise FileNotFoundError("File doesnt exist ({})".format(input_file))

        extension = input_file.split(".")[-1]
        learningset = None

        if extension == "json":
            learningset = self._parse_from_json(input_file)
        elif extension in ["xlsx"]:
            learningset = self._parse_from_xlsx(input_file)
        elif extension in ["xls"]:
            raise NotImplementedError("old excel file is not managed for now")
        elif extension in ["ods"]:
            raise NotImplementedError("openoffice is not managed for now")
        else:
            raise AssertionError("extension not managed {}".format(extension))

        logger.debug("found {} colums : {}".format(len(learningset.variables), learningset.variables))
        logger.debug("found {} patterns : {}".format(len(learningset.patterns), learningset.patterns))

        return learningset