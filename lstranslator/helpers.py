# coding=UTF-8

"""
    Function to help to parse / write LearningSet
"""

import csv
from os import path as ospath
from lstranslator.parser.ucr import UcrParser
from lstranslator.parser.orange import OrangeParser
from lstranslator.parser.internal import InternalParser
from lstranslator.parser.old import OldParser
from lstranslator.parser.arff import ArffParser
from lstranslator.writer.orange import OrangeWriter
from lstranslator.writer.internal import InternalWriter
from lstranslator.writer.ucr import UcrWriter
from lstranslator.helper.logger import logger
from lstranslator.writer.arff import ArffWriter
import shutil
import tempfile

# inspired from a post on stackeroverflow
def query_yes_no(question, default=True):
    """Ask a yes/no question via standard input and return the answer.

    If invalid input is given, the user will be asked until
    they acutally give valid input.

    Args:
        question(str):
            A question that is presented to the user.
        default(bool|None):
            The default value when enter is pressed with no value.
            When None, there is no default value and the query
            will loop.
    Returns:
        A bool indicating whether user has entered yes or no.

    Side Effects:
        Blocks program execution until valid input(y/n) is given.
    """
    yes_list = ["yes", "y"]
    no_list = ["no", "n"]

    default_dict = {  # default => prompt default string
        None: "[y/n]",
        True: "[Y/n]",
        False: "[y/N]",
    }

    default_str = default_dict[default]
    prompt_str = "%s %s " % (question, default_str)

    while True:
        choice = input(prompt_str).lower()

        if not choice and default is not None:
            return default
        if choice in yes_list:
            return True
        if choice in no_list:
            return False

        notification_str = "Please respond with 'y' or 'n'"
        print(notification_str)

def get_parser(parser_name):
    """In looking on the parser name, get the right parser.

    The parser has to implements the IParser interface.

    Arguments:
        parser_name {str} -- name of the parser

    Raises:
        KeyError -- the given name dosent correspond to any other parsers

    Returns:
        IParser -- parser
    """

    parser = None
    if parser_name == 'orange':
        parser = OrangeParser()
    elif parser_name == 'internal':
        parser = InternalParser()
    elif parser_name in ['one_file', 'ucr']:
        parser = UcrParser()
    elif parser_name == 'old':
        parser = OldParser()
    elif parser_name == 'arff':
        parser = ArffParser()
    else:
        raise KeyError('{} parser is not found'.format(parser_name))
    return parser

def get_writer(writer_name):
    """In looking on the writer name, get the right writer.

    The parser has to implements the IWriter interface.

    Arguments:
        writer_name {[type]} -- [description]

    Raises:
        KeyError -- the given name dosent correspond to any other writers

    Returns:
        IWriter -- writer
    """

    writer = None
    if writer_name in ['orange3', 'orange']:
        writer = OrangeWriter(version='orange3')
    elif writer_name == 'orange2':
        writer = OrangeWriter(version='orange2')
    elif writer_name == 'internal':
        writer = InternalWriter()
    elif writer_name == 'ucr':
        writer = UcrWriter()
    elif writer_name == 'arff':
        writer = ArffWriter()
    else:
        raise KeyError('{} writer is not found'.format(writer_name))
    return writer

def detect_ls_format(path):
    """Function to detect the LearningSet format in looking on the given path.

    Arguments:
        path {str} -- location of the learning set

    Raises:
        IOError -- data file not found
        KeyError -- format not found

    Returns:
        str -- learningset format
    """

    if not ospath.exists(path):
        raise IOError('not file found')
    if ospath.splitext(path)[-1] == '.tab':
        return 'orange'
    elif ".xls" in path or ".ods" in path or "profile.json" in path:
        return 'internal'
    elif "_raw.json" in path:
        return 'old'
    elif ".arff" in path:
        return 'arff'
    else:
        with open(path) as csvfile:
            rows = list(csv.reader(csvfile, delimiter=",",
                                   quotechar='"', quoting=csv.QUOTE_ALL))
            if len(rows) > 1:
                val = rows[0][0]
                if str(val).isdigit():
                    return "ucr"
                else:
                    raise KeyError('format not found')

def parse(path, data_format="auto"):
    """Function to parse a file using the given data format.

    If data format is "auto", it will try to detect format using helper.

    Arguments:
        path {str} -- path where to find the dataset

    Keyword Arguments:
        data_format {str} -- data format (default: {"auto"})

    Returns:
        LearningSet -- parsed data
    """

    if data_format == "auto":
        data_format = detect_ls_format(path)

    return get_parser(data_format).parse_from_path(path)

def write(data_format, learningset, destination, autogenerate=False, mode_data="json", mode_sequences="auto"):
    """Common function to store the learningset in a file following specific data format, data mode and sequences mode.

    If the destination folder (file) already exists, ask by a prompt if we overwrite it.

    Arguments:
        data_format {str} -- output format
        learningset {LearningSet} -- object to write
        destination {str} -- folder path where it will be written.

    Keyword Arguments:
        autogenerate {bool} -- autogenerate the folder (default: {False})
        mode_data {str} -- how main data will be written (default: {"json"})
        mode_sequences {str} -- how the sequences will be written (default: {"auto"})
    """

    if data_format == "internal":
        # if the path already exists, ask user to know if we overwrite it, if not stop
        if (ospath.exists(destination)):
            if query_yes_no("Folder \"{}\" already exists, do you want to overwrite it ?".format(destination)):
                # create a backup
                tmp_path_name = tempfile.mkdtemp()
                shutil.move(destination, ospath.join(destination, tmp_path_name))
                try:
                    result = get_writer(data_format).write(learningset, destination, autogenerate, mode_data, mode_sequences)
                    # delete backup
                    shutil.rmtree(ospath.join(destination, tmp_path_name))
                    logger.debug('conversion done')
                    return result
                except Exception as e:
                    logger.error(e)
                    # restore old path
                    shutil.move(ospath.join(destination, tmp_path_name), destination)
        else:
            get_writer(data_format).write(learningset, destination, autogenerate, mode_data, mode_sequences)
            logger.debug('conversion done')
    else:
        get_writer(data_format).write(learningset, destination)
        logger.debug('conversion done')
