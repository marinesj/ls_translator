# coding=UTF-8

"""Adapters to be used to link translater to another sructure"""

from lstranslator.helper.logger import logger

try:
    from decisiontree.core import LearningSet as Ls
    from decisiontree.core import Variable as Vrb
    from decisiontree.core import Observation as Obs
    from decisiontree.core import Type as Tp
except ImportError:
    logger.warning('decisiontree package is not found, please install it')

from lstranslator.models.timeseries import TimeSeries

def adapt_to_tdt_learning_set(learningset_model):
    """Custom adapter to link translator with ikats work (LIG)

    Arguments:
        learningset_model {LearningSet} -- given learningset object

    Returns:
        object -- lig object
    """

    #variables = [Vrb.Variable(ls_name, 'float', Tp.Type.TIMESERIES, (-5, 5))]
    variables = [Vrb.Variable(v.name, v.type, Tp.Type[str(v.imp_type.value).upper()], v.range) for v in learningset_model.variables]
    observations = []
    ts_names = dict()
    for line_id, obs in enumerate(learningset_model.observations):
        data = []
        for cell in obs.data:
            if isinstance(cell, TimeSeries):
                data.append(cell.data)
                ts_names[(line_id, cell.name)] = cell.id
            else:
                data.append(cell)
        observations.append(Obs.Observation(obs.id, data, obs.label))
    """ts_names = dict()

    observations = []
    with open(input_file, 'r') as _file:
        lines = _file.readlines()
        line_id = 0
        nb_digit = len(str(len(lines)))
        for line in lines:
            _arr = line.split(delimiter)
            data = []
            if len(_arr) > 1:
                label = str(_arr[0])
                obs_id = obs_prefix + str(line_id).zfill(nb_digit)
                ts_name = ts_prefix + str(line_id).zfill(nb_digit)
                data.append(np.array([x for x in _arr[1:]], dtype=np.float64))
                observation = Obs.Observation(obs_id=obs_id, data=data, label=label)
                observations.append(observation)
                ts_names[(line_id, ls_name)] = ts_name
            line_id += 1
    """
    return Ls.LearningSet(name=learningset_model.name,
                            observations=observations,
                            variables=variables,
                            ts_names=ts_names)