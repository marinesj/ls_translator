"""This module centralizes logger management

By default it uses the constant DEFAULT_VERBOSITY to know how the verbosity logger will be

It has two default handlers:
  - a file called lstranslator.log
  - a console handler

During initialization, call update_logger method
"""

import logging
import colorlog

from lstranslator.config import DEFAULT_VERBOSITY, LEVEL_VERBOSITY_DISABLED, LEVEL_VERBOSITY_ENABLED

logger = logging.getLogger("lstranslator")

def update_logger(verbosity):
    """Update the logger instance to enable/disable logger verbosity

    Arguments:
        verbosity {bool} -- [description]
    """

    global logger
    if verbosity:
        logger.setLevel(LEVEL_VERBOSITY_ENABLED)
    else:
        logger.setLevel(LEVEL_VERBOSITY_DISABLED)

# CONSOLE
handler_console = colorlog.StreamHandler()
handler_console.setFormatter(colorlog.ColoredFormatter("[%(asctime)s][%(log_color)s%(levelname)s%(reset)s] %(message)s"))
handler_console.setLevel(logging.DEBUG)
logger.addHandler(handler_console)

## LOGGER FILE
handler_file = logging.FileHandler("lstranslator.log", mode="a", encoding="utf-8")
handler_file.setFormatter(logging.Formatter("[%(asctime)s][%(name)s][%(levelname)s] %(message)s (%(filename)s:%(lineno)d)"))
handler_file.setLevel(logging.DEBUG)
logger.addHandler(handler_file)

# update verbosity
update_logger(DEFAULT_VERBOSITY)
