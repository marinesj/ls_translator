"""This module manages ``UCR`` writing."""

from os import path as ospath
import csv

from lstranslator.writer.interface import IWriter
from lstranslator.enum.variabletype import VariableType

class UcrWriter(IWriter):
    """This class manages learningset save as ``UCR`` ls format"""

    def __init__(self):
        self._obs_prefix = 'obs_'
        self._ts_prefix = 'TS_'
        self._delimiter = ','

    def write(self, learning_set, destination):
        """ write learning set as ``UCR`` format in a specific path

        Arguments:
            learning_set {LearningSet} -- learning set to write
            destination {string} -- destination path, can be a folder or a file
        """
        # init array
        rows = []

        seq_variables = [v.name for v in learning_set.variables if v.imp_type == VariableType.TIMESERIES]
        if len(seq_variables) > 1:
            raise ValueError('too many sequences, only one is allowed')
        elif len(seq_variables) == 0:
            raise ValueError('at least one sequence variable is neeed')

        label_variables = [v.name for v in learning_set.variables if v.has_meta('class')]
        if len(label_variables) > 1:
            raise ValueError('too many labels, only one is allowed')

        seq_key = seq_variables[0]
        label_key = label_variables[0]

        for observation in learning_set.observations:
            # add data from observation + its label
            rows.append([observation.get_value(label_key)] + observation.get_value(seq_key).data.tolist())

        # construct filepath
        if ospath.isdir(destination):
            # construct filepath
            filepath = ospath.join(destination, learning_set.name)
        else:
            filepath = destination

        with open(filepath, 'w') as orange_file:
            # prepare csv writer
            #  delimiter is a tab
            writer = csv.writer(orange_file, delimiter=self._delimiter, quoting=csv.QUOTE_MINIMAL)
            for row in rows:
                # write the row using the writer
                writer.writerow(row)
