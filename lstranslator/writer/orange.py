"""Module managing ``ORANGE`` writing
"""

import csv
from os import path as ospath

from lstranslator.models.learningset import LearningSet
from lstranslator.enum.gdftype import GDFType
from lstranslator.enum.domain import Domain

from lstranslator.writer.interface import IWriter

class OrangeWriter(IWriter):
    """Class to write an ``ORANGE`` dataset from a Learningset.
    """

    def __init__(self, version='orange2'):
        """Constructor

        Keyword Arguments:
            version {str} -- version of ORANGE (default: {'orange2'}, values: [{'orange2'}, {'orange3'}])
        """

        self._version = version

    def _header_as_orange2(self, variables):
        """Transform GDF variables as **orange 2** variables (headers)

        Arguments:
            variables {list} -- list of variables

        Returns:
            list -- header
        """

        variable_names = []
        variable_types = []
        variable_metadatas = []
        for variable in variables:
            variable_names.append(variable.name)
            if variable.has_meta('class'):
                variable_metadatas.append('class')
            else:
                variable_metadatas.append('')
            if variable.type == GDFType.NUMBER:
                variable_types.append('c')
            elif variable.type == GDFType.TIMESTAMP:
                variable_types.append('t')
            # TODO: check
            elif variable.type in [GDFType.ORDERED, GDFType.ENUMERATE]:
                variable_types.append('d')
            # TODO: check
            elif variable.type == GDFType.STRING:
                variable_types.append('s')

        #variable_names.append('class')
        #variable_types.append('d')
        #variable_metadatas.append('class')

        return [variable_names, variable_types, variable_metadatas]

    def _header_as_orange3(self, variables):
        """Transform GDF variables as **orange 3** variables (headers)

        Arguments:
            variables {list} -- list of variables

        Returns:
            list -- header
        """

        header = []
        for variable in variables:
            attributes = ''
            if variable.has_meta('class'):
                attributes += 'c'
            if variable.type == GDFType.NUMBER:
                attributes += 'C'
            elif variable.type == GDFType.TIMESTAMP:
                attributes += 'T'
            elif variable.type in [GDFType.ORDERED, GDFType.ENUMERATE]:
                attributes += 'D'
            elif variable.type == GDFType.STRING:
                attributes += 'S'
            header.append('#'.join([attributes, variable.name]))

        #header.append('cD#class')
        return [header]

    def write(self, learning_set, destination):
        """ Write learning set as ```ORANGE``` format in a specific path

        .. note::
            It writes in following version property **orange2** or **orange3**

        Arguments:
            learning_set {LearningSet} -- learning set to write
            destination {string} -- destination path, can be a folder or a file
        """
        # init array
        rows = []

        variables = [variable for variable in learning_set.variables if not variable.has_meta('fake')]

        if self._version == 'orange2':
            # replace array by one with the header
            rows = self._header_as_orange2(variables)
        else:
            # replace array by three (names, types, metadatas)
            rows = self._header_as_orange3(variables)

        for observation in learning_set.observations:
            # add data from observation + its label
            tmp_row = [observation.data[i] for i, v in enumerate(observation.variables) if not v.has_meta('fake')]
            rows.append(tmp_row)

        # construct filepath
        if ospath.isdir(destination):
            # construct filepath
            filepath = ospath.join(destination, learning_set.name + '.tab')
        else:
            filepath = destination

        with open(filepath, 'w') as orange_file:
            # prepare csv writer
            #  delimiter is a tab
            writer = csv.writer(orange_file, delimiter='\t', quoting=csv.QUOTE_MINIMAL)
            for row in rows:
                # write the row using the writer
                writer.writerow(row)
