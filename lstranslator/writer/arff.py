"""This module manages ``WEKA`` writing

.. note::
    Writing ``WEKA`` format is equivalent as writing docuemnt with **.arff** extension
"""

import os, arff

from lstranslator.writer.interface import IWriter
from lstranslator.enum.gdftype import GDFType
from lstranslator.enum.domain import Domain
from lstranslator.enum.variabletype import VariableType
from lstranslator.helper.logger import logger


class ArffWriter(IWriter):
    """Class to write a ``WEKA`` dataset from a Learningset.

    Implements IWriter interface.
    """

    def __init__(self, version='orange2'):
        self._version = version
        self._extension_file = ".arff"

    def write(self, learning_set, destination):
        """Write the learning set following ``WEKA`` **arff** format in a specific path

        Arguments:
            learning_set {LearningSet} -- learningset to write
            destination {string} -- destination path, can be a folder or a file
        """
        # init array
        attributes = []
        data = []

        for variable in learning_set.variables:

            # skip auto create variable
            if variable.has_meta('fake') or variable.imp_type in [VariableType.TIMESERIES, VariableType.PATTERN]:
                logger.warn("{} is not managed by ArffWriter, skipped..".format(variable))
                continue

            if variable.type == variable.type == GDFType.ENUMERATE:
                attributes.append((variable.name, variable.range))
            elif variable.type == GDFType.NUMBER:
                attributes.append((variable.name, 'NUMERIC'))
            elif variable.domain == Domain.FLOAT:
                attributes.append((variable.name, 'REAL'))
            elif variable.type in [GDFType.ENUMERATE,  GDFType.ORDERED]:
                attributes.append((variable.name, variable.range))
            else:
                attributes.append((variable.name, variable.type.name))

        for observation in learning_set.observations:
            obs_data = []
            for variable_name, extra in attributes:
                obs_data.append(observation.get_value(variable_name))
            data.append(obs_data)

        raw_data = {
            "attributes": attributes,
            "data": data,
            "relation": learning_set.name,
            "description": learning_set.description
        }

        # construct
        if os.path.isdir(destination):
            # construct
            filename = os.path.join(destination, learning_set.name + self._extension_file)
        else:
            filename = destination

        with open(filename, 'w') as file:
            # write json object in the json_file
            dump_data = arff.dumps(raw_data)
            print(dump_data, file=file)
