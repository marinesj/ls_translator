"""Writer in ``GDF`` format.

This format is made in order to gather all variables/data used in machine learning.

.. note::
    It can be called as **GDF** or **internal** or **transional** data format.
"""

import csv, json, os, uuid

from collections import OrderedDict
from datetime import datetime

from openpyxl import Workbook
from openpyxl.styles import Font, PatternFill
from openpyxl.utils import get_column_letter

from lstranslator.writer.interface import IWriter
from lstranslator.enum.variabletype import VariableType
from lstranslator.models.timeseries import TimeSeries
from lstranslator.enum.domain import Domain

from lstranslator.helper.logger import logger

from lstranslator.config import LS_VERSION, COLUMNS, VARIABLE_SHEET_KEY, SEQUENCE_SHEET_KEY,\
                                PATTERN_SHEET_KEY, DATA_DAF_SHEET_KEY, DATA_GDF_SHEET_KEY

RGB_FORMAT = "{0:02X}{1:02X}{2:02X}"
DEFAULT_FONT = Font(name="Calibri", size=12)

class NoIndent(object):
    """ Class to identify object to encode in one line """
    def __init__(self, value):
        self.value = value

class NoIndentEncoder(json.JSONEncoder):
    """ Encoder to manage NoIndent object"""
    def __init__(self, *args, **kwargs):
        super(NoIndentEncoder, self).__init__(*args, **kwargs)
        self.kwargs = dict(kwargs)
        del self.kwargs['indent']
        self._replacement_map = {}

    def default(self, o):
        if isinstance(o, NoIndent):
            key = uuid.uuid4().hex
            self._replacement_map[key] = json.dumps(o.value, **self.kwargs)
            return "@@%s@@" % (key,)
        else:
            return super(NoIndentEncoder, self).default(o)

    def encode(self, o):
        result = super(NoIndentEncoder, self).encode(o)
        for k, v in self._replacement_map.items():
            result = result.replace('"@@%s@@"' % (k,), v)
        return result

def _deep_parsing(value):
    """Function to parse value and manage NoIndent object

    Arguments:
        value {object} -- value

    Returns:
        object -- parsed value
    """

    if isinstance(value, NoIndent):
        value = value.value
    if isinstance(value, list) or isinstance(value, tuple):
        value = ','.join([str(_deep_parsing(v)) for v in value])
    return value

#@profile
def _save_as_xlsx_spreadsheet(sheets, destination, name, formats):
    """Function to save sheets in a xlsx spreadsheet file

    Arguments:
        sheets {dict} -- dictionnary containing sheets that will be saved in the spreadsheet
        destination {str} -- destination path where the file will be saved
        name {str} -- name of file
        formats {dict} -- [description]
    """


    wb = Workbook(write_only=True)
    def _set_value(sheet, i, j, value, font, color=None):
        cell = sheet.cell(row=i, column=j)
        cell.value = value
        cell.font = font
        if color:
            cell.fill = color

    for key, rows in sheets.items():
        logger.debug('create sheet {}'.format(key))
        worksheet = wb.create_sheet(key)
        rgb = [180,199,231]
        main_title_colour = RGB_FORMAT.format(*rgb)

        header_rgb = [218, 227, 243]
        header_colour = RGB_FORMAT.format(*header_rgb)

        headers = formats.get(key)
        if isinstance(rows, list):
            logger.debug('begin set {} rows'.format(len(rows)))
            if (headers):
                worksheet.append([])
                worksheet.row_dimensions[1].fill = PatternFill(patternType="solid", fgColor=main_title_colour, bgColor=main_title_colour)
                worksheet.row_dimensions[2].fill = PatternFill(patternType="solid", fgColor=header_colour, bgColor=header_colour)
                worksheet.append(headers)

            for row in rows:
                if isinstance(row, list):
                    worksheet.append(row)
                elif isinstance(row, OrderedDict):
                    worksheet.append([_deep_parsing(row.get(header)) for header in headers])
            logger.debug("ending set rows")
        else:
            for key1 in rows.keys():
                row = [key1]
                values = rows[key1]
                if not(isinstance(values, list)):
                    values = [values]
                row += values
                worksheet.append(row)

    logger.debug('saving {} ...'.format(os.path.join(destination, name + '.xlsx')))
    wb.save(os.path.join(destination, name + '.xlsx'))

class InternalWriter(IWriter):
    """
        Class to write a ``GDF`` dataset from a Learningset.
    """

    def __init__(self):
        self._delimiter = '\t'
        self._extension_file = '.csv'
        self._profile_filename = 'profile.json'
        self._by_obs_limit = 3
        self._folder_patterns = 'patterns'
        self._folder_sequences = 'variables'
        self._folder_observations = 'observations'
        self._line_indicator = '#'
        self._columns = COLUMNS

    def _write_csv_file(self, file_path, rows):
        """write csv file

        Arguments:
            file_path {[type]} -- [description]
            rows {[type]} -- [description]
        """

        os.makedirs(os.path.dirname(file_path), exist_ok=True)
        with open(file_path, 'w') as csv_file:
            # prepare csv writer
            #  delimiter is a tab
            writer = csv.writer(csv_file, delimiter=self._delimiter, quoting=csv.QUOTE_MINIMAL)
            for row in rows:
                # write the row using the writer
                writer.writerow(row)

    def _compute_variables(self, ls_variables):
        """Function to transform each variable descriptors to dict

        Arguments:
            ls_variables {list} -- list of variables

        Returns:
            list -- list of variables as dictionnary
        """

        """
        variables: [
            {
                *name: [var1, var2, ...],
                *type: 'number|boolean|timestamp|string|enumerate|...',
                *domain: 'integer|float|...',
                range: [...]
                units: 'm'
                relevance: 2.0
                comment: 'other information about these variables',
                meta:
            }, ...]
        """
        variables = []
        ls_variables = [v for v in ls_variables if v.imp_type not in [VariableType.TIMESERIES, VariableType.PATTERN]]
        variable = OrderedDict()
        for ls_variable in list(ls_variables):
            variable = OrderedDict()
            variable["name"] = ls_variable.name
            variable["type"] = str(ls_variable.type.value)
            variable["domain"] = str(ls_variable.domain.value)
            if ls_variable.range:
                variable["range"] = NoIndent(ls_variable.range)
            if ls_variable.units and ls_variable.units != '_':
                variable["units"] = ls_variable.units
            if ls_variable.relevance:
                variable["relevance"] = ls_variable.relevance
            variable["comment"] = ''
            if isinstance(ls_variable.comment, str):
                variable["comment"] = ls_variable.comment
            variable["meta"] = NoIndent(ls_variable.metas)
            variables.append(variable)
        return variables

    def _compute_sequences(self, ls_variables):
        """Function to transform earch sequence descriptors to dict

        Arguments:
            ls_variables {list} -- list of descriptors

        Returns:
            list -- liste of sequences as dictionnary
        """

        """
        sequences: [
        {
            *name: [seq1, seq2, ...],
            *type: 'number|boolean|timestamp|string|enumerate|...',
            *domain: 'integer|float|...',
            range: [ ...],
            units: 'kg',
            *index: [timestamp_var|mark_variable]|'integer',
            *sampling: either a number, 'uneven' or 'unknown',
            relevance: 2.0 // the relevance is expressed as a float, default value is 1.0
            weighted: 'true|false'
            comment: 'other information about these sequence',
            meta: [['ignore'] ['skip'] [inline]]
        }, ...]
        """
        sequences = []
        seq_variables = [v for v in ls_variables if v.imp_type is VariableType.TIMESERIES]

        for seq_variable in seq_variables:
            sequence = OrderedDict()
            sequence["name"] = seq_variable.name
            sequence["type"] = str(seq_variable.type.value)
            sequence["domain"] = str(seq_variable.domain.value)
            if seq_variable.units and seq_variable.units != '_':
                sequence["units"] = NoIndent(seq_variable.units)
            if seq_variable.range:
                sequence["range"] = NoIndent(seq_variable.range)
            if seq_variable.sampling:
                sequence["sampling"] = seq_variable.sampling
            if seq_variable.index:
                sequence["index"] = seq_variable.index
            sequence["meta"] = NoIndent(seq_variable.metas)
            sequences.append(sequence)
        return sequences

    def _compute_patterns(self, ls_variables):
        """Function to transform each pattern descriptors to dict

        Arguments:
            ls_variables {list} -- list of descriptors

        Returns:
            list -- liste of patterns as dictionnary
        """

        """
            {
                names:     [pat1, pat2, ...],
                interpreter:   ['RHM', 'version', 'parameter1', ...],
                definition:    ['[AB].$H[EF]', '[AB][CD]$.[BC]', ...],
                variables:     ['speed', 'angle_dr'],
                *slice:    [start_point, end_point],
                variants:  [exist, count, fst_pos,...],
                type:      'number|boolean|timestamp|string|enumerate|...',
                *domain:   [ ...],
                *breakpoints:  [[-5.6, -4, 0, 2], [-4, 1, 12, 18]],
                *vocabulary:   [[], ['<-4°','1°','12°',>18°']],
                length:    [2, 2, ...],
                *comment:  'other information about these patterns',
                *meta:     [['class'] ['id'] ['ignore'] ...]
            }
        """

        patterns = []
        pat_variables = [v for v in ls_variables if v.imp_type is VariableType.PATTERN]
        patterns_tmp = OrderedDict()
        for p in pat_variables:
            if not patterns_tmp.get(p.description.name):
                patterns_tmp[p.description.name] = p.description

        _by_variables = OrderedDict()
        _types_by_pat_name = { p.description.name: p.type.value for p in pat_variables}

        for pat_tmp in patterns_tmp.values():
            row = _by_variables.setdefault(str(pat_tmp.break_points), [])
            row.append(pat_tmp)

        for group in _by_variables.values():
                pat_ex = group[0]
                pattern = OrderedDict()
                pattern['name'] = NoIndent([p.name for p in group])
                pattern['generator'] = pat_ex.interpreter
                pattern['definition'] = NoIndent([p.regex for p in group])
                pattern['sequences'] = NoIndent(pat_ex.variables)
                if pat_ex.slice:
                    pattern['slice'] = pat_ex.slice
                pattern['indicators'] = NoIndent(pat_ex.variants)
                pattern['breakpoints'] = [ NoIndent(v) for v in pat_ex.break_points]
                pattern['vocabulary'] = [ NoIndent(v) for v in pat_ex.vocabulary]
                #pattern['length'] = NoIndent([int(p.length) for p in group])
                pattern['comment'] = ''
                pattern['meta'] = []
                patterns.append(pattern)
        return patterns

    def write(self, learning_set, destination, autogenerate=False, mode_data="json", mode_sequence="auto"):
        """Write the learning set as *internal* ``GDF`` format in a specific folder

        Arguments:
            learning_set {[type]} -- learning set to write
            destination {[type]} -- destination path

        Keyword Arguments:
            autogenerate {bool} -- if destination folder exists, overwrite it, otherwise throw an error (default: {False})
            mode_data {str} -- how data will be stored (default: {"json"})
            mode_sequence {str} -- how sequence will be stored (default: {"auto"})

        Raises:
            IOError -- path doesnt exist
            IOError -- path should be a dir
        """

        # check destination
        if not os.path.exists(destination):
            if autogenerate:
                logger.debug('autogenerate path {}'.format(destination))
                os.makedirs(destination, exist_ok=True)
            else:
                raise IOError('path doesnt exist {}'.format(destination))
        elif not os.path.isdir(destination):
            raise IOError('path should be a dir')

        logger.info("write learningset %s in GDF format with %d observations, %d TS, %d patterns", learning_set.name, len(learning_set.observations), len(learning_set.timeseries), len(learning_set.patterns))

        seq_variables = [x for x in learning_set.variables if x.imp_type is VariableType.TIMESERIES]
        by_obs = False
        daf = False

        if mode_sequence == "observation":
            by_obs = True
        elif mode_sequence == "variable":
            by_obs = False
        elif mode_sequence == "daf":
            daf = True
        else:
            by_obs = len(seq_variables) > len(learning_set.observations)

        rows = []

        ### HEADER
        rows.append([variable.name for variable in learning_set.variables])

        ### PREPARE CONTENT
        timeseries = {variable.name: [] for variable in seq_variables}
        patterns = {'_'.join(variable.name.split('/')): [] for variable in learning_set.variables if variable.imp_type is VariableType.PATTERN and '/pos' in variable.name}
        for line_id, observation in enumerate(learning_set.observations):
            row = []
            matrix = None
            for variable in learning_set.variables:
                if variable.imp_type is VariableType.TIMESERIES:
                    # format as list of float
                    ts_data = [float(x) for x in observation.get_value(variable.name).data.tolist()]
                    # keep data in an dict with variable name as key
                    timeseries.get(variable.name).append(ts_data)
                    if by_obs:
                        row.append(os.path.join(self._folder_observations, observation.id + self._extension_file))
                    elif not daf:
                        row.append(self._line_indicator.join([
                            os.path.join(self._folder_sequences, variable.name + self._extension_file),
                            str(line_id)
                        ]))
                    else:
                        row.append(ts_data)
                elif variable.imp_type is VariableType.PATTERN:
                    pattern_name, variant = variable.name.split('/')
                    if variant == 'pos':
                        # format filename
                        filename = '_'.join([pattern_name, variant])
                        # keep observation data in an dict with filename as key (pattern0_pos)
                        patterns.get(filename).append(observation.get_value(variable.name))
                        if not daf:
                            row.append(self._line_indicator.join([
                                os.path.join(self._folder_patterns, filename + self._extension_file),
                                str(line_id)
                            ]))
                        else:
                            row.append(observation.get_value(variable.name))
                    else:
                        row.append(observation.get_value(variable.name))
                else:
                    row.append(observation.get_value(variable.name))

            if not daf:
                rows.append(row)
            else:
                maxlen = max([len(v) if isinstance(v, list) else 1 for v in row]) # find the max vector length
                matrix = [[None] * len(learning_set.variables) for i in range(maxlen)] # prepare daf matrix for the observation
                for j, v in enumerate(row):
                    if isinstance(v, list): # means sequence / array
                        for i, v1 in enumerate(v):
                            matrix[i][j] = v1 # loop on the list and set the value
                    else:
                        matrix[0][j] = v # set direct value
                rows += matrix

        ## PROFILE
        profile = OrderedDict()

        # header
        header = OrderedDict()
        header["doctype"] = ["GDF", str(LS_VERSION)]
        header["date"] = datetime.now().strftime("%Y-%m-%d %H:%M:%S") # 'yy-mm-dd hh:mm:ss'
        header["dataset"] = learning_set.name # "official name of the dataset"
        header["source"] = ''
        header["authors"] = learning_set.authors
        #header["learningSet"] = [] #[learning_set.name]
        profile["header"] = header

        # descriptors
        features = OrderedDict()

        # variables
        features["variables"] = self._compute_variables(learning_set.variables)
        # sequence
        features["sequence"] = self._compute_sequences(learning_set.variables)
        # patterns
        features["patterns"] = self._compute_patterns(learning_set.variables)

        profile["features"] = features

        ### WRITE FILES
        os.makedirs(destination, exist_ok=True)
        if not daf:
            ## TS
            if by_obs:
                headers = [[s.name for s in seq_variables]]
                for observation in learning_set.observations:
                    # prepare ts_data (cast value)
                    ts_data = [[float(x) if s.domain == Domain.FLOAT else int(x) for x in observation.get_value(s.name).data.tolist()] for s in seq_variables]
                    maxlen = max([len(t) for t in ts_data])
                    matrix = [[None] * len(seq_variables) for i in range(maxlen)]
                    for i, seq_variable in enumerate(seq_variables):
                        for j, value in enumerate(ts_data[i]):
                            matrix[j][i] = value
                    self._write_csv_file(os.path.join(destination, self._folder_observations,
                                                observation.id + self._extension_file),
                                headers + matrix)
            else:
                for seq_variable in seq_variables:
                    self._write_csv_file(os.path.join(destination, self._folder_sequences,
                                                seq_variable.name + self._extension_file),
                                    timeseries.get(seq_variable.name, []))
            # patterns
            for pattern_key, pattern in patterns.items():
                self._write_csv_file(os.path.join(destination, self._folder_patterns,
                                            pattern_key + self._extension_file),
                                pattern)

        if mode_data == 'json':
            ## main XXX.csv
            self._write_csv_file(os.path.join(destination, learning_set.name + self._extension_file), rows)

            with open(os.path.join(destination, self._profile_filename), 'w') as json_file:
                # write json object in the json_file
                data = json.dumps(profile, indent=4, cls=NoIndentEncoder)
                print(data, file=json_file)

        elif mode_data in ['spreadsheet_with_data', 'spreadsheet_without_data']:
            sheets = OrderedDict() # use to keep how we insert data
            sheets["header"] = profile["header"]
            sheets[VARIABLE_SHEET_KEY] = features.get("variables")
            sheets[SEQUENCE_SHEET_KEY] = features.get("sequence")
            sheets[PATTERN_SHEET_KEY] = features.get("patterns")
            if mode_data == 'spreadsheet_with_data':
                key = DATA_GDF_SHEET_KEY if not daf else DATA_DAF_SHEET_KEY
                # in this case we add the data as a sheet from the spreadsheet
                sheets[key] = rows
            else:
                # in this case, we want the data external from the spreadsheet
                ## main XXX.csv
                self._write_csv_file(os.path.join(destination, learning_set.name + self._extension_file), rows)
            # save the xlsx file
            _save_as_xlsx_spreadsheet(sheets, destination, learning_set.name, self._columns)
