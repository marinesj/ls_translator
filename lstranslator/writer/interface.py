"""Writer Interface for any writer that lstranslator can manage
"""

import abc

class IWriter(abc.ABC):
    """ Class used as an interface.

    It implements abc.ABC interface """

    @abc.abstractmethod
    def write(self, learning_set, destination, autogenerate=False):
        """ Abstract method of write """
        pass
