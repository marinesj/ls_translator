# coding=UTF-8

import numpy as np

from lstranslator.enum.variabletype import VariableType
from lstranslator.enum.domain import Domain
from lstranslator.enum.gdftype import GDFType
from lstranslator.helper.logger import logger

from lstranslator.config import COMPLIANCES

def _is_compliant(v_type, v_domain):
    """
    Control if v_type is compliant with v_domain
    :param v_type: type of the variable (gdftype)
    :param v_domain: domain of the variable (float, integer...)
    :type v_type: GDFType
    :type v_domain: Domain
    :return: boolean
    :rtype: boolean
    """

    for gdftypes, domains in COMPLIANCES:
        if v_type in gdftypes:
            return v_domain in domains

    logger.warning('type not found {}'.format(v_type))

    return False

class Column(object):
    """
        Class abstraite équivalente à une variable
    """
    def __init__(self, name, v_type, domain, imp_type, v_range, comment="", metas=[]):
        """Constructor

        Arguments:
            name {str} -- name of the variable
            v_type {GDFType} -- Main type
            domain {Domain} -- Domain linked to the type
            imp_type {VariableType} -- Implementation type as {variable|timeseries|pattern}
            v_range {list} -- Range of the variable depending on the v_type and domain

        Keyword Arguments:
            comment {str} -- (default: {""})
            metas {list} -- list of keywords that allows to mark data (default: {[]})

        Raises:
            ValueError -- sampling is mandatory
            ValueError -- v_type is mandatory
            ValueError -- domain is mandatory
            ValueError -- implementation type is mandatory

        Returns:
            Column -- object
        """

        if not name:
            raise ValueError('<Column {}> sampling is mandatory'.format(name))
        if not v_type:
            raise ValueError('<Column {}> v_type is mandatory'.format(name))
        if not domain:
            raise ValueError('<Column {}> domain is mandatory'.format(name))
        if not imp_type:
            raise ValueError('<Column {}> imp_type is mandatory'.format(name))

        assert isinstance(v_type, GDFType), "Value Type of {} is unknown ({})".format(name, v_type)
        assert isinstance(domain, Domain), "Domain of {} is unknown ({})".format(name, domain)
        assert isinstance(imp_type, VariableType), "Implementation type of {} is unknown ({})".format(name, imp_type)
        assert _is_compliant(v_type, domain), "Domain {} is not compliant with {}".format(v_type, domain)

        self._name = name
        self._type = v_type
        self._domain = domain
        self._imp_type = imp_type
        self._range = v_range
        self._comment = comment
        self._metas = metas
        if isinstance(self.metas, str):
            self._metas = [[self._metas]]

    # Getters and Setters
    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        self._name = value

    @property
    def type(self):
        return self._type

    @type.setter
    def type(self, value):
        self._type = value

    @property
    def domain(self):
        return self._domain

    @domain.setter
    def domain(self, value):
        self._domain = value

    @property
    def imp_type(self):
        return self._imp_type

    @imp_type.setter
    def imp_type(self, value):
        self._imp_type = value

    @property
    def range(self):
        return self._range

    @range.setter
    def range(self, value):
        self._range = value

    @property
    def comment(self):
        return self._comment

    @comment.setter
    def description(self, value):
        self._description = value

    @property
    def metas(self):
        return self._metas

    @metas.setter
    def metas(self, value):
        self._metas = value

    def add_meta(self, key, value = None):
        meta = [key]
        if value:
            meta.append(key)

    def has_meta(self, key):
        for meta in self._metas:
            if meta[0] == key:
                return True
        return False

    def __repr__(self):
        return "<Column {} ({}, {}, {})>".format(self.name, self.type, self._domain, self._metas)

class PatternColumn(Column):
    def __init__(self, name, v_type, domain, generator, definition, sequences, v_slice=None, indicator=None, breakpoints=None, vocabulary=None, relevance=1.0, comment="", metas=[]):
        """Constructor

        Arguments:
            name {str} -- [description]
            v_type {GDFType} -- [description]
            domain {Domain} -- [description]
            generator {object} -- [description]
            definition {object} -- [description]
            sequences {list} -- [description]

        Keyword Arguments:
            v_slice {list} -- [description] (default: {None})
            indicator {object} -- [description] (default: {None})
            breakpoints {list} -- [description] (default: {None})
            vocabulary {list} -- [description] (default: {None})
            relevance {float} -- [description] (default: {1.0})
            comment {str} -- [description] (default: {""})
            metas {list} -- [description] (default: {[]})

        Raises:
            ValueError -- generator is mandatory
            ValueError -- definition is mandatory
            ValueError -- sequences is mandatory

        Returns:
            Pattern Column -- object
        """

        super(PatternColumn, self).__init__(name=name, v_type=v_type, domain=domain, comment=comment, metas=metas, v_range=None, imp_type=VariableType.PATTERN)

        # controls
        if not generator:
            raise ValueError('<Column {}> generator is mandatory'.format(name))
        if not definition:
            raise ValueError('<Column {}> definition is mandatory'.format(name))
        if not sequences:
            raise ValueError('<Column {}> sequences is mandatory'.format(name))

        self._generator = generator
        self._definition = definition
        self._sequences = sequences
        self._slice = v_slice
        self._indicator = indicator
        self._breakpoints = breakpoints
        self._vocabulary = vocabulary
        self._relevance = relevance

    # Getters and Setters
    @property
    def generator(self):
        return self._generator

    @property
    def definition(self):
        return self._definition

    @property
    def sequences(self):
        return self._sequences

    @property
    def slice(self):
        return self._slice

    @property
    def indicator(self):
        return self._indicator

    @property
    def breakpoints(self):
        return self._breakpoints

    @property
    def vocabulary(self):
        return self._vocabulary

    @property
    def relevance(self):
        return self._relevance

    def __repr__(self):
        return "<PatternColumn {} => {} ({}, {})>".format(self.name, self.definition, self.sequences, self.indicator)

class VariableColumn(Column):
    def __init__(self, name, v_type, domain, v_range=None, units=None, relevance=1.0, comment="", metas=[]):
        """constructor

        Arguments:
            name {str} -- [description]
            v_type {GDFType} -- [description]
            domain {Domain} -- [description]

        Keyword Arguments:
            v_range {list} -- [description] (default: {None})
            units {str} -- [description] (default: {None})
            relevance {float} -- [description] (default: {1.0})
            comment {str} -- [description] (default: {""})
            metas {list} -- [description] (default: {[]})

        Returns:
            VariableColumn -- object
        """

        super(VariableColumn, self).__init__(name=name, v_type=v_type, domain=domain, comment=comment, metas=metas, v_range=v_range, imp_type=VariableType.VARIABLE)

        # controls
        self._units = units
        self._relevance = relevance

    @property
    def units(self):
        return self._units

    @property
    def relevance(self):
        return self._relevance

class SequenceColumn(Column):
    def __init__(self, name, v_type, domain, index, sampling, v_range=None, units=None, relevance=1.0, weighted=False, comment="", metas=[]):
        """constructor

        Arguments:
            name {str} -- [description]
            v_type {GDFType} -- [description]
            domain {Domain} -- [description]
            index {str} -- [description]
            sampling {str} -- [description]

        Keyword Arguments:
            v_range {list} -- [description] (default: {None})
            units {str} -- [description] (default: {None})
            relevance {float} -- [description] (default: {1.0})
            weighted {bool} -- [description] (default: {False})
            comment {str} -- [description] (default: {""})
            metas {list} -- [description] (default: {[]})

        Raises:
            ValueError -- sampling is mandatory
            ValueError -- index is mandatory

        Returns:
            SequenceColumn -- object
        """

        super(SequenceColumn, self).__init__(name=name, v_type=v_type, domain=domain, comment=comment, metas=metas, v_range=v_range, imp_type=VariableType.TIMESERIES)

        # controls
        if not sampling:
            raise ValueError('<SequenceColumn {}> sampling is mandatory'.format(name))
        if not index:
            raise ValueError('<SequenceColumn {}> index is mandatory'.format(name))

        self._units = units
        self._index = index
        self._sampling = sampling
        self._relevance = relevance
        self._weighted = weighted

    # Getters and Setters
    @property
    def units(self):
        return self._units

    @property
    def index(self):
        return self._index

    @property
    def sampling(self):
        return self._sampling

    @property
    def relevance(self):
        return self._relevance

    @property
    def weighted(self):
        return self._weighted