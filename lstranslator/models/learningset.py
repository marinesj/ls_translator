# coding=UTF-8
from lstranslator.models.profile import GDFProfile

"""
    Class LearningSet
"""

class LearningSet(object):
    """
    Holds the learning set, ie both the series and the labels,
    if in a supervised setting.
    Data are stored as numpy arrays."""

    def __init__(self, observations, timeseries, labels, variables, features, header = None, name='learningset', patterns = [], description= ''):
        """Initializes the learning set.

        Arguments:
            observations {list} -- list of Observation
            timeseries {list} -- list of Timeseries
            labels {list} -- list of labels
            variables {list} -- list of Column
            features {list} -- list of feature // same as Column

        Keyword Arguments:
            header {dict} -- [description] (default: {None})
            name {str} -- [description] (default: {'learningset'})
            patterns {list} -- [description] (default: {[]})
            description {str} -- [description] (default: {''})
        """

        self._name = name
        self._version = "0.24"

        self._profile = GDFProfile(name, "lambda", description= description, features=features, variables=variables, labels=labels)

        self._data = {
            'observations': observations,
            'timeseries': timeseries,
            'patterns': patterns
        }

    @property
    def name(self):
        """ getting name """
        return self._name

    @property
    def variables(self):
        """ getting array of variables """
        return self._profile.variables

    @property
    def description(self):
        """ getting array of timeseries """
        return self._profile.description

    @property
    def authors(self):
        """ getting array of timeseries """
        return self._profile.authors

    @property
    def features(self):
        """ getting array of timeseries """
        return self._profile.features

    @property
    def observations(self):
        """ getting array of observations"""
        return self._data.get('observations')

    @property
    def patterns(self):
        """ getting array of timeseries """
        return self._data.get('patterns')

    @property
    def timeseries(self):
        """ getting array of timeseries """
        return self._data.get('timeseries')