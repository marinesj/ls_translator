# coding=UTF-8

import numpy as np

from lstranslator.enum.variabletype import VariableType

class TimeSeries(object):
    """

    Implements multivariate time series with uniform time stamps.

    .. warning::

        The type of the element should be np.float64. If the values parameter is a list, \
        it will be converted to the correct type (there is duplication), else there is no duplication,\
        assuming the caller **will** provide an np.float64 ndarray.
        In this version (spark), we have to deal with a TimeSeries object instead of Learningset,
        and thus, we added the label (unique for each ts) and the id (tsuid).

    """

    def __init__(self, values, ts_id, time_stamps, obs_id, name, duplicate=False):
        """Initialize Timeseries

        Arguments:
            values {np.ndarray} -- the values of the timeseries
            ts_id {str} -- timeseries identifier
            time_stamps {np.ndarray} -- array of the timestamps (same format as values). \
        if not set, ts is supposed to be uniform. Non uniform timestamps are not yet managed.
            obs_id {str} -- linked observation identifier
            name {str} -- name of the timeseries

        Keyword Arguments:
            duplicate {bool} -- should we copy the data or just link them (default not copy). (default: {False})
        """

        if isinstance(values, list) or isinstance(values, np.ndarray):
            self.__values = np.array(values, dtype=np.float64, copy=duplicate)
        #if time_stamps is not None:
        #    raise NotImplementedError("timeseries with non uniform timestamps are not yet managed")
        self._id = ts_id
        self._obs_id = obs_id
        self._name = name
        self._meta_type = VariableType.TIMESERIES
        self._v_type = 'float'
        self._time_stamps = time_stamps

    def __str__(self):
        #Returns a printable version of the TS.
        return '\n'.join(["values: ", str(self.__values),
                          "shape: ", str(self.__values.shape)])

    def __getitem__(self, pos_x):
        """ returns the xth value, which can be the array of the values of the n variables"""
        return self.__values[pos_x]

    @property
    def data(self):
        """ public property on readable """
        return self.__values

    @property
    def value_type(self):
        """ public property on readable """
        return self._v_type

    @property
    def type(self):
        """ public property on readable """
        return self._meta_type

    @property
    def id(self):
        """ public property on readable """
        return self._id

    @property
    def obs_id(self):
        """ public property on readable """
        return self._id

    @property
    def timestamps(self):
        """ public property on readable """
        return self._time_stamps

    def time_len(self):
        """
        :return: the number of values in the first series
        :rtype: int
        """
        return self.__values.shape[0]

    @property
    def name(self):
        """getter for the name field.
        :return: the name
        :rtype: str
        """
        return self._name

    def min(self):
        """getter for the name field.
        :return: the name
        :rtype: str
        """
        return np.amin(self.__values)

    def max(self):
        """getter for the name field.
        :return: the name
        :rtype: str
        """
        return np.amax(self.__values)

    def to_json(self):
        """
        Convert the object to a json readable object
        """
        return dict(name=self._name, values=self.__values.tolist())
