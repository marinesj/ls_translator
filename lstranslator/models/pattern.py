# coding=UTF-8

import numpy as np

class Pattern(object):
    """

    Implements pattern.

    """

    def __init__(self, name, regex, variables, break_points, length,
                 interpreter=[], variants=[], locations={},
                 vocabulary=None, slice_type=None, sampling=None):
        """Initialize pattern

        Arguments:
            name {str} -- [description]
            regex {str} -- [description]
            variables {list} -- [description]
            break_points {list} -- [description]
            length {[type]} -- [description] // OBSOLETE

        Keyword Arguments:
            interpreter {list} -- [description] (default: {[]})
            variants {list} -- [description] (default: {[]})
            locations {dict} -- [description] (default: {{}})
            vocabulary {list} -- [description] (default: {None})
            slice_type {str} -- [description] (default: {None})
            sampling {str} -- [description] (default: {None})

        Returns:
            Pattern -- object
        """

        self._name = name
        self._regex = regex
        self._variables = variables
        self._break_points = break_points
        self._length = length
        self._interpreter = interpreter
        self._vocabulary = vocabulary
        self._variants = variants
        self._slice = slice_type
        self._sampling = sampling
        self._locations = locations

    @property
    def name(self):
        """ public property on readable """
        return self._name

    @property
    def break_points(self):
        """ public property on readable """
        return self._break_points

    @property
    def interpreter(self):
        """ public property on readable """
        return self._interpreter

    @property
    def regex(self):
        """ public property on readable """
        return self._regex

    @property
    def variables(self):
        """ public property on readable """
        return self._variables

    @property
    def slice(self):
        """ public property on readable """
        return self._slice

    @property
    def sampling(self):
        """ public property on readable """
        return self._sampling

    @property
    def variants(self):
        """ public property on readable """
        return self._variants

    @property
    def vocabulary(self):
        """ public property on readable """
        return self._vocabulary

    @property
    def length(self):
        """ public property on readable """
        return self._length

    @property
    def locations(self):
        """ public property on readable """
        return self._locations

    @locations.setter
    def locations(self, value):
        self._locations = value

    def __repr__(self):
        return "<Pattern {}, {} ({}, {})>".format(self.name, self.regex, self.variables, self.variants)