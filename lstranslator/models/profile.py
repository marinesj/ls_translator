# coding=UTF-8

import numpy as np
from datetime import datetime
from lstranslator.config import LS_VERSION

class GDFProfile(object):
    """

    Implements Profile

    """

    def __init__(self, name, source,
                 doctype = ['GDF', LS_VERSION], date = datetime.now(), description = '',
                 variables = [], features = [], authors = '', learningsets = [], labels = []):
        """Initialize profile

        Arguments:
            name {str} -- [description]
            source {str} -- [description]

        Keyword Arguments:
            doctype {list} -- [description] (default: {['GDF', LS_VERSION]})
            date {date} -- [description] (default: {datetime.now()})
            description {str} -- [description] (default: {''})
            variables {list} -- [description] (default: {[]})
            features {list} -- [description] (default: {[]})
            authors {str} -- [description] (default: {''})
            learningsets {list} -- [description] (default: {[]})
            labels {list} -- [description] (default: {[]})
        """

        self._name = name
        self._date = date
        self._source = source

        self._doctype = doctype
        self._features = features
        self._variables = variables
        self._authors = authors
        self._learningsets = learningsets
        self._description = description

        self._labels = labels
    @property
    def name(self):
        """ getting name """
        return self._name

    @property
    def date(self):
        """ getting name """
        return self._date

    @property
    def source(self):
        """ getting name """
        return self._source

    @property
    def doctype(self):
        """ getting array of timeseries """
        return self._doctype

    @property
    def description(self):
        """ getting array of timeseries """
        return self._description

    @property
    def variables(self):
        """ getting array of variables """
        return self._variables

    @property
    def features(self):
        """ getting array of timeseries """
        return self._features

    @property
    def authors(self):
        """ getting array of observations"""
        return self._authors

    @property
    def labels(self):
        """ getting array of timeseries """
        return self._labels

    @property
    def learningsets(self):
        """ getting array of timeseries """
        return self._learningsets