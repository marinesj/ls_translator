# coding=UTF-8

from lstranslator.enum.variabletype import VariableType
from lstranslator.enum.domain import Domain

"""
    Observation Model
"""

class Observation(object):
    """
        Class representing an :any:`Observation` (or called Example)

        .. warning::

            It will store data and variables as list with same length.

    """

    def __init__(self, obs_id, variables, data):
        """Initialisation

        Arguments:
            obs_id {string} -- identifier
            variables {list} -- list of Column or variable
            data {list} -- list of values

        Raises:
            ValueError -- obs_id is mandatory
            ValueError -- length of values list and variables list is not the same
        """

        self._obs_id = obs_id

        if self._obs_id is None:
            raise ValueError('<Observation> obs_id is mandatory')

        self._variables = variables or [] # by default init an empty list
        self._data = data or [] # by default init an empty list

        if len(self._variables) != len(self._data):
            raise ValueError('<Observation {}>, variables and data list doesnt have same length {} vs {}'.format(self._obs_id, len(self._variables), len(self._data)))

        """
        for i, value in enumerate(data):
            if value is not None:
                variable = variables[i]
                if v.type == VariableType.VARIABLE:
                    if v.domain == Domain.INTEGER:
                        if not isinstance(d, int):
                            print('not integer value', d, v)
                    elif (v.domain == Domain.FLOAT):
                        if not isinstance(d, float):
                            print('not float value', d)
        """

        self._hash_data = {v.name: data[i] for i, v in enumerate(variables)}

    @property
    def data(self):
        """Property that gives data

        Returns:
            list -- list of values
        """
        return self._data

    @property
    def id(self):
        """ Property to identify the observation """
        return self._obs_id

    @property
    def variables(self):
        """ Property listing variables """
        return self._variables

    @property
    def label(self):
        """ Property listing labels (or class) """
        labels = [self._hash_data[v.name] for v in self._variables if v.has_meta('class')]
        is_empty = len(labels) == 0
        if not is_empty:
            return labels

    def get_value(self, variable_name=None, variable=None):
        """ Function to get single value looking on criterias (variable or variable name)

        Keyword Arguments:
            variable {Column} -- Variable object (default: {None})
            variable_name {string} -- Variable name (default: {None})

        Returns:
            object -- value
        """
        if variable:
            return self._hash_data[variable.name]
        elif variable_name:
            return self._hash_data[variable_name]

    def has_variable(self, variable=None, variable_name=None):
        """Function to check if variable exists in this observation

        Keyword Arguments:
            variable {Column} -- Variable object (default: {None})
            variable_name {string} -- Variable name (default: {None})

        Returns:
            boolean -- {True|False}
        """
        return self.get_value(variable=variable, variable_name=variable_name) is not None

    def __repr__(self):
        """Override Observation representation

        Returns:
            str -- representation as a string
        """

        return "<Observation {}>".format(self._obs_id)
