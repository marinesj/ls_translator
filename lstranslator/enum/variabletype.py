# coding=UTF-8

from enum import Enum

class VariableType(Enum):
    """
    An enumeration of the possible kind of feature we might have. (in term of implementation)

    * **CONTINUOUS**: a range of values
    * **TIMESERIES**: Timeseries of continuous values
    * **PATTERN**: same as continuous but has more information

    .. note::
        It helps to distingish :any:`Column`

    """

    TIMESERIES = 'timeseries'
    VARIABLE = 'variable'
    PATTERN = 'pattern'
