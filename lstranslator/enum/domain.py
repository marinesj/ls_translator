# coding=UTF-8

from enum import Enum

class Domain(Enum):
    """
    An enumeration of the possible domain we might have.

    * **INTEGER**: An integer.
    * **FLOAT**: A float.
    * **STRING**: String of any length or sequence of unicode characters.
    * **UNIX_TIME**.
    * **ISO_8601**: "A string close to the ISO 8601 format: « yyyy-mm-dd hh:mm:ss:ms »".
    * **NONE**: Nothing.

    .. note::

        It will be used combined with :any:`GDFType` to define the type of a :any:`Column`

    """

    INTEGER = 'integer'
    FLOAT = 'float'
    STRING = 'string'
    UNIX_TIME = 'unix_time'
    ISO_8601 = 'iso_8601'
    NONE = 'none'