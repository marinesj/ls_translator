from enum import Enum

class GDFType(Enum):
    """
    An enumeration of the possible types we use in ``GDF``.

    * **NUMBER**: A number that can be either a float or an integer.
    * **MODULAR**
    * **TIMESTAMP**: An integer corresponding a Unix timestamp.
    * **DATE**: "A string close to the ISO 8601 format: « yyyy-mm-dd hh:mm:ss:ms »".
    * **BOOLEAN**: Classical boolean, the possible values are either {0, 1} or {false, true}.
    * **MARK**
    * **ORDERED**: Set of ordered value.
    * **ENUMERATE**: Set of unordered value.
    * **STRING**: String of any length.
    * **HIERARCHY**
    * **PICTURE**
    * **NONE**: Nothing.

    .. note::

        It will be used combined with :any:`Domain` to define the type of a :any:`Column`
    """

    NUMBER = 'number'
    MODULAR = 'modular'
    TIMESTAMP = 'timestamp'
    DATE = 'date'
    BOOLEAN = 'boolean'
    MARK = 'mark'
    ORDERED = 'ordered'
    ENUMERATE = 'enumerate'
    STRING = 'string'
    HIERARCHY = 'hierarchy'
    PICTURE = 'picture'
    NONE = 'none'