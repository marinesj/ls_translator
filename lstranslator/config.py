"""Config module to share constants"""

from lstranslator.enum.domain import Domain
from lstranslator.enum.gdftype import GDFType
import logging

### GENERAL ###

# VERSION
LS_VERSION = 0.46 #: linked version used from GDF spec
# LOGGER
DEFAULT_VERBOSITY = False #: The verbosity used at initialization
LEVEL_VERBOSITY_DISABLED = logging.WARNING
LEVEL_VERBOSITY_ENABLED = logging.DEBUG

### SPREADSHEET ###

DEFAULT_SHEET_SUFFIX = 'data'
VARIABLE_SHEET_KEY = DEFAULT_SHEET_SUFFIX + '.var'
SEQUENCE_SHEET_KEY = DEFAULT_SHEET_SUFFIX + '.seq'
PATTERN_SHEET_KEY = DEFAULT_SHEET_SUFFIX + '.pat'
DATA_GDF_SHEET_KEY = DEFAULT_SHEET_SUFFIX + '.gdf'
DATA_DAF_SHEET_KEY = DEFAULT_SHEET_SUFFIX + '.daf'

# COLUMNS USED TO COMPUTE THE SHEETS
COLUMNS = {
    VARIABLE_SHEET_KEY: ["name", "type", "domain", "range", "units", "relevance", "comment", "meta"],
    "data.seq": ["name", "type", "domain", "range", "units", "relevance", "index", "sampling", "weighted", "comment", "meta"],
    "data.pat": ["name", "generator", "definition", "sequences", "indicators", "breakpoints", "vocabulary", "relevance", "comment", "meta"]
} #: headers of each kind of descriptors (variable, sequences, patterns)

# PARSE VALUE WITH A LINEID
LINE_SEPARATORS = ["#", ":"] #: used in data table to compute "file_path" + "line_separator" + "line_id"

### CSV ###

# DELIMITERSS
CSV_DELIMITER = [';']  #: used as default delimiter
ALLOWED_CSV_DELIMITERS = [';', ',', '\t']  #: list of allowed csv delimiters that accepts parsers

### COLUMNS ###

# COMPLIANCES between listed Domains and GDFType
COMPLIANCES = [
    [
        [GDFType.NUMBER, GDFType.MODULAR, GDFType.MARK],
        [Domain.INTEGER, Domain.FLOAT]
    ],
    [
        [GDFType.ENUMERATE],
        [Domain.STRING]
    ],
    [
        [GDFType.DATE],
        [Domain.UNIX_TIME, Domain.ISO_8601]
    ],
    [
        [GDFType.TIMESTAMP],
        [Domain.INTEGER, Domain.FLOAT, Domain.UNIX_TIME, Domain.ISO_8601]
    ],
    [
        [GDFType.ORDERED],
        [Domain.INTEGER, Domain.STRING]
    ],
    [
        [GDFType.HIERARCHY],
        [Domain.STRING]
    ],
    [
        [GDFType.BOOLEAN, GDFType.STRING, GDFType.PICTURE],
        [Domain.NONE]
    ]
]  #: list of allowed combination between GDFType and Domain