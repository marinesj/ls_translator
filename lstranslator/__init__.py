"""
lstranslator

The ls-translator module implements functions to read and write ARFF files in
Python.

It was created in the Grenoble Computer Science Laboratory
(LIG), which takes place at Grenoble, France.
"""