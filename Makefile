.SILENT:

.SHELL:
SHELL = /bin/bash

.PHONY: help test doc

PACKAGE := lstranslator
# Assumes Python3
pip3_version := $(shell pip3 --version 2>/dev/null)

default: help

## Install package
install:
ifdef pip3_version
	python3 setup.py install
else
	@echo "error: please install the \`pip3' package"
	@exit 0
endif

## Clean all generated files
clean:
	find -name "__pycache__" | xargs rm -rf
	find -name "*.pyc" | xargs rm -f
	find -name "*.pyd" | xargs rm -f
	find -name "*.pyo" | xargs rm -f
	#find -name "*.orig" | xargs rm -fi
	-@rm -rf build/
	-@rm -rf dist/
	-@rm -rf *.egg-info/
	-@rm -rf reports/

## Generate documnetation
doc:
	sphinx-apidoc -fMPle -o sphinx/_apidoc/ lstranslator
	rm ./sphinx/_apidoc/modules*
	rm -rf ./docs
	cd ./sphinx && (make html)

## Install package + environnement
fullinstall:
ifdef pip3_version
	pip3 install -r requirements.txt
	make install
else
	@echo "error: please install the \`pip3' package"
	@exit 0
endif

## Display this help text
help:
	printf "\n"
	printf "Available targets\n\n"
	awk '/^[a-zA-Z\-\_0-9]+:/ { \
		helpMessage = match(lastLine, /^## (.*)/); \
		if (helpMessage) { \
			helpCommand = substr($$1, 0, index($$1, ":")-1); \
			helpMessage = substr(lastLine, RSTART + 3, RLENGTH); \
			printf "%-15s %s\n", helpCommand, helpMessage; \
		} \
	} \
	{ lastLine = $$0 }' $(MAKEFILE_LIST)
	printf "\n"

## Uninstall package
uninstall:
	pip3 uninstall $(PACKAGE)

## Run testsuite
test:
	rm -rf ./reports
	pytest --cov-report html:reports/coverage --cov lstranslator --verbose --html=reports/report.html --self-contained-html
