LS-TRANSLATOR
=========

The ls-translator module implements functions to read and write ARFF files in
Python. It was created in the Grenoble Computer Science Laboratory
(LIG), which takes place at Grenoble, France.

`ARFF <https://weka.wikispaces.com/ARFF+(stable+version)>` (Attribute-Relation File Format) is an file format specially created for
describe datasets which are used commonly for machine learning experiments and
softwares. This file format was created to be used in Weka, the best
representative software for machine learning automated experiments.

`ORANGE <https://docs.orange.biolab.si/3/data-mining-library/tutorial/data.html>` is a file format used for `Orange software <https://orange.biolab.si/>`. It is a famous component-based data mining software including a range of data visualization, exploration, preprocessing and modeling techniques. The two versions (orange2 and orange3) are managed by the translator.

UCR file format.

GDF file format, file . (spreasheet and json mode)

Old file format (almost obsolete)

VERSION
-------

0.46

Features
--------

- Read and write ARFF / Orange / UCR / GDF / Old files using python built-in structures, such dictionaries
  and lists;
- Fully compatible with Python 3.3+;
- Under `MIT License <http://opensource.org/licenses/MIT>`_

Requirements
------------

You need Python 3 or later work run ls_translator.  You can have multiple Python
versions (2.x and 3.x) installed on the same system without problems.

In Ubuntu, Mint and Debian you can install Python 3 like this:

    $ sudo apt-get install python3 python3-pip

For other Linux flavors, OS X and Windows, packages are available at

  http://www.python.org/getit/

Dependencies
------------

- numpy
- liac_arff
- argcomplete
- openpyxl

How To Install
--------------

Using direct repository :

    # clone repository
    $ git clone https://gricad-gitlab.univ-grenoble-alpes.fr/marinesj/ls_translator.git
    $ cd ./ls_translator
    # install package
    $ python setup.py install

Using pip :

    $ pip3 install git+https://gricad-gitlab.univ-grenoble-alpes.fr/marinesj/ls_translator.git

Usage
-----

```bash
usage: ls_translator [-h] -s SOURCE -d DESTINATION
                     [-f {internal,arff,old,ucr,orange2,orange3}]
                     [-t {internal,arff,ucr,orange2,orange3}] [-a]

optional arguments:
  -h, --help            show this help message and exit
  -s SOURCE, --source SOURCE
                        entry file
  -d DESTINATION, --destination DESTINATION
                        To path
  -f {internal,arff,old,ucr,orange2,orange3}, --from_format {internal,arff,old,ucr,orange2,orange3}
                        from format
  -t {internal,arff,ucr,orange2,orange3}, --to {internal,arff,ucr,orange2,orange3}
                        to format
  -a                    autogenerate destination folder
  -v                    debug verbosity
  -ms {auto,observation,variable,onefileline,onefilecolumn}, --mode_store_sequences {auto,observation,variable,onefileline,onefilecolumn}
                        mode store sequences
  -md {json,spreadsheet}, --mode_store_data {json,spreadsheet}
                        mode store data
```

    $ ls_translator -s ./test/internal/BirdChicken/profile.json -f internal -d ./examples/ -t orange3
    or
    $ ./bin/ls_translator -s ./test/data/arff/car.arff -f arff -d ./examples/ -t internal

Tests
-----

    python testsuite.py # run main instruction

or
    pip3 install pytest
    pytest  # collect all tests and run them
    pytest -k 'arff' # run only test with arff keyword

Contributors
------------

- `Julien Marinescu (davisp1) <https://gitab.com/davisp1>, (marinesj) <https://gricad-gitlab.univ-grenoble-alpes.fr/marinesj/>`

Project Page
------------

<https://gricad-gitlab.univ-grenoble-alpes.fr/marinesj/ls_translator>