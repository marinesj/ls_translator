from setuptools import setup, find_packages

print(find_packages())
setup(name='lstranslator',
      version='0.1',
      description='Learningset parser/writer for several dataformats (internal / ucr / orange / viz)',
      url='https://gitlab.com/davisp1/ikats_parser',
      author='Julien Marinescu',
      author_email='julien.marinescu@gmail.com',
      license='MIT',
      packages=find_packages(),
      setup_requires=[],
      include_package_data=True,
      install_requires=["numpy", "liac-arff", "argcomplete", "openpyxl", "colorlog"],
      scripts=['bin/lstranslator'],
      zip_safe=False)
