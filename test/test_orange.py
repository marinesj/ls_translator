# coding=UTF-8

"""
    Suite tests to check if orange parser/writer works fine
"""

import os
import shutil
from lstranslator.parser.orange import OrangeParser
from lstranslator.writer.orange import OrangeWriter
from lstranslator.parser.internal import InternalParser
from lstranslator.writer.internal import InternalWriter

TEST_PATH = os.path.dirname(os.path.realpath(__file__))
DATA_PATH = os.path.join(TEST_PATH, 'data', 'orange')
TMP_PATH = os.path.join(TEST_PATH, 'tmp')
DEST_PATH = os.path.join(TEST_PATH, 'new')

def _check_content_files(file_path_1, file_path_2):
    """ Generic function to check content from two files

    Arguments:
        file_path_1 {string} -- first file to compare
        file_path_2 {string} -- second file to compare
    """
    source_file = open(file_path_1, "r")
    destination_file = open(file_path_2, "r")
    # get lines from files
    lines1 = source_file.readlines()
    lines2 = destination_file.readlines()
    # close files
    source_file.close()
    destination_file.close()
    # should have same number of lines
    assert (len(lines1) is len(lines2)), \
        "Converted and original files have not same number of lines"
    # each line should be equal
    for i, line in enumerate(lines1):
        assert (line == lines2[i]), \
        "Converted and original files are not same, some lines are differents \n {} {}" \
        .format(line, lines2[i])

def _test_orange_x(file_name, version, use_internal_format=False):
    """ Generic function to test orange

    Arguments:
        file_name {string} -- file name to find DATA_PATH
        version {string} -- orange version (orange2 | orange3)
    """
    file_source_path = os.path.join(DATA_PATH, file_name)
    # parse learning set
    learningset = OrangeParser().parse_from_path(file_source_path)
    # create dest_path
    os.makedirs(TMP_PATH, exist_ok=True)
    os.makedirs(DEST_PATH, exist_ok=True)

    # if use_internal_format is True, pass by internal format before write finalized format
    if use_internal_format:
        file_destination_path = os.path.join(TMP_PATH, 'profile.json')
        # write parsed learningset in another path
        InternalWriter().write(learningset, TMP_PATH)
        learningset = InternalParser().parse_from_path(file_destination_path)

    file_destination_path = os.path.join(DEST_PATH, file_name)
    # write parsed learningset in another path
    OrangeWriter(version=version).write(learningset, DEST_PATH)

    # check the two files
    _check_content_files(file_source_path, file_destination_path)
    # remove DEST_PATH
    shutil.rmtree(DEST_PATH)
    shutil.rmtree(TMP_PATH)

def test_orange_2(use_internal_format=False):
    """ Test orange2 usecase """
    _test_orange_x(file_name='iris_v2.tab', version='orange2', use_internal_format=use_internal_format)

def test_orange_3(use_internal_format=False):
    """ Test orange3 usecase """
    _test_orange_x(file_name='iris_v3.tab', version='orange3', use_internal_format=use_internal_format)
