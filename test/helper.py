# coding=UTF-8

from lstranslator.parser.internal import InternalParser
from lstranslator.writer.internal import InternalWriter


def _assert_variables(variables1, variables2):
    assert (len(variables1) == len(variables2)), \
                                             "Number of variables is not the same \n {} {}" \
                                             .format(len(variables1), len(variables2))
    vars2_as_dict = {obs.name: obs for obs in variables2}
    for variable1 in variables1:
        variable2 = vars2_as_dict.get(variable1.name)
        assert (variable2 is not None), 'Variable not found in the output {}'.format(variable1.name)
        # TODO how to check if we compare tuple and array?
        assert (variable1.range == variable2.range or tuple(variable1.range) == tuple(variable2.range)), 'Variable has not same range "{}" ({}) vs ({})'.format(variable1.name, variable1.range, variable2.range)
        assert (variable1.type == variable2.type), 'Variable has not same type "{}" ({}) vs ({}) ({})'.format(variable1.name, variable1.type, variable2.name, variable2.type)
        assert (variable1.imp_type == variable2.imp_type), 'Variable has not same implementation type {}'.format(variable1.imp_type)
        assert (variable1.metas == variable2.metas), 'Variable has not same meta {}'.format(variable1.metas)


def _assert_observations(observations1, observations2):
    assert (len(observations1) == len(observations2)), \
                                             "Number of observations is not the same \n {} {}" \
                                             .format(len(observations1), len(observations2))
    obs2_as_dict = {obs.id: obs for obs in observations2}
    for obs in observations1:
        obs1 = obs2_as_dict.get(obs.id)
        assert (obs1 is not None), 'Observation not found in the output {}'.format(obs.id)
        assert (obs.label == obs1.label), 'Observation label is not equal {}'.format(obs.id)
        assert (obs.label == obs1.label), 'Observation label is not equal {}'.format(obs.id)

        _assert_variables(obs.variables, obs1.variables)
        for v in obs.variables:
            value1 = obs.get_value(v.name)
            value2 = obs1.get_value(v.name)
            assert(value1.__class__ == value2.__class__), 'Data type is not equal {} {} {} {} {}'.format(obs.id, type(value1), type(value2), value1, value2)
            if v.imp_type.value == 'timeseries':
                value1 = value1.data.tolist()
                value2 = value2.data.tolist()
            assert(value1 == value2), 'Data is not equal "{}" "{}" ({}) vs ({})'.format(obs.id, v.name, value1, value2)


def check_learning_sets(learningset, learningset2):
    _assert_variables(learningset.variables, learningset2.variables)
    _assert_observations(learningset.observations, learningset2.observations)

    assert (len(learningset.timeseries) == len(learningset2.timeseries)), \
                                             "Number of variables is not the same \n {} {}" \
                                             .format(len(learningset.timeseries), len(learningset2.timeseries))

def check_content_files(file_path_1, file_path_2):
    check_learning_sets(InternalParser().parse_from_path(file_path_1), InternalParser().parse_from_path(file_path_2))
