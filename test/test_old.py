# coding=UTF-8

"""
    Suite tests to check if orange parser/writer works fine
"""

import os
import shutil
from lstranslator.parser.old import OldParser
from lstranslator.parser.internal import InternalParser
from lstranslator.writer.internal import InternalWriter
from test.helper import check_learning_sets

TEST_PATH = os.path.dirname(os.path.realpath(__file__))
DATA_PATH = os.path.join(TEST_PATH, 'data', 'old')
DEST_PATH = os.path.join(TEST_PATH, 'new')


def test_old():
    filename = 'BirdChicken_raw.json'
    file_source_path = os.path.join(DATA_PATH, filename)

    # parse learning set (old)
    old_learningset = OldParser().parse_from_path(file_source_path)

    # create dest_path
    os.makedirs(DEST_PATH, exist_ok=True)

    # use internal format to check contents
    # write parsed learningset in another path (internal)
    file_destination_path = os.path.join(DEST_PATH, 'profile.json')
    InternalWriter().write(old_learningset, DEST_PATH)

    # parse new files
    gdf_learningset = InternalParser().parse_from_path(file_destination_path)

    # check contents
    check_learning_sets(old_learningset, gdf_learningset)

    # write parsed learningset in another path
    # remove DEST_PATH
    shutil.rmtree(DEST_PATH)
