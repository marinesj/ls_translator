# coding=UTF-8

"""
    Suite tests to check if orange parser/writer works fine
"""

import os
import shutil
from lstranslator.parser.ucr import UcrParser
from lstranslator.writer.ucr import UcrWriter
from lstranslator.writer.internal import InternalWriter
from lstranslator.parser.internal import InternalParser

TEST_PATH = os.path.dirname(os.path.realpath(__file__))
DATA_PATH = os.path.join(TEST_PATH, 'data', 'ucr')
DEST_PATH = os.path.join(TEST_PATH, 'new')
TMP_PATH  = os.path.join(TEST_PATH, 'tmp')

def _check_content_files(file_path_1, file_path_2):
    source_file = open(file_path_1, "r")
    destination_file = open(file_path_2, "r")
    lines1 = source_file.readlines()
    lines2 = destination_file.readlines()
    source_file.close()
    destination_file.close()
    assert (len(lines1) is len(lines2)), \
        "Converted and original files have not same number of lines"
    for i, line in enumerate(lines1):
        cells1 = line.split(',')
        cells2 = lines2[i].split(',')
        assert (len(cells1) == len(cells2)), \
            "Converted and original files have not same number of lines => {} vs {} in line {}" \
            .format(len(cells1), len(cells2), i)
        for j, cell in enumerate(cells1):
            # TODO: should we check pure raw value or its float representation ?
            # (float representation for now)
            assert (float(cell) == float(cells2[j])), \
                "Converted and original files have not same content" \
                " => {} vs {} in line {}; col {}"\
                .format(cell, cells2[j], i, j)

def test_ucr():
    """ Test ucr usecase """
    file_name = 'BirdChicken_TRAIN'
    file_source_path = os.path.join(DATA_PATH, file_name)
    learningset = UcrParser().parse_from_path(file_source_path)
    os.makedirs(DEST_PATH, exist_ok=True)
    os.makedirs(TMP_PATH, exist_ok=True)

    file_destination_path = os.path.join(TMP_PATH, 'profile.json')
    # write parsed learningset in another path
    InternalWriter().write(learningset, TMP_PATH)
    learningset = InternalParser().parse_from_path(file_destination_path)

    file_destination_path = os.path.join(DEST_PATH, file_name)
    UcrWriter().write(learningset, DEST_PATH)
    _check_content_files(file_source_path, file_destination_path)
    shutil.rmtree(DEST_PATH)
    shutil.rmtree(TMP_PATH)
