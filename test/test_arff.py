# coding=UTF-8

"""
    Suite tests to check if orange parser/writer works fine
"""

import os
import shutil
from test.helper import check_learning_sets
import arff
from lstranslator.parser.arff import ArffParser
from lstranslator.writer.arff import ArffWriter
from lstranslator.parser.internal import InternalParser
from lstranslator.writer.internal import InternalWriter

TEST_PATH = os.path.dirname(os.path.realpath(__file__))
DATA_PATH = os.path.join(TEST_PATH, 'data', 'arff')
TMP_PATH = os.path.join(TEST_PATH, 'tmp')
DEST_PATH = os.path.join(TEST_PATH, 'new')

def test_arff():
    filename = 'car.arff'
    file_source_path = os.path.join(DATA_PATH, filename)

    # create folders
    os.makedirs(TMP_PATH, exist_ok=True)
    os.makedirs(DEST_PATH, exist_ok=True)

    # parse source learning set
    source_learningset = ArffParser().parse_from_path(file_source_path)

    # write parsed learningset in another path
    InternalWriter().write(source_learningset, TMP_PATH)

    # read transition parsed learninget set
    tmp_destination_path = os.path.join(TMP_PATH, 'profile.json')
    tmp_learningset = InternalParser().parse_from_path(tmp_destination_path)

    # write final learning set
    final_destination_path = os.path.join(DEST_PATH, filename)
    ArffWriter().write(tmp_learningset, final_destination_path)

    # check arff contents using liad-arff
    with open(file_source_path, 'r') as file:
        initial_arff = arff.load(file)
    with open(final_destination_path, 'r') as file:
        final_arff = arff.load(file)
    assert initial_arff.get('attributes') == final_arff.get('attributes'), "Attributes not equal"
    assert initial_arff.get('relation') == final_arff.get('relation'), "Relation not equal"
    assert initial_arff.get('data') == final_arff.get('data'), "Data not equal"

    # parse final learning set (arff)
    final_learningset = ArffParser().parse_from_path(final_destination_path)

    # check learning set content between source and final
    check_learning_sets(source_learningset, final_learningset)

    # remove DEST_PATH
    shutil.rmtree(DEST_PATH)
    shutil.rmtree(TMP_PATH)