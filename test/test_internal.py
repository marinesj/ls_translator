# coding=UTF-8

"""
    Suite tests to check if orange parser/writer works fine
"""

import shutil
import os
from lstranslator.parser.internal import InternalParser
from lstranslator.writer.internal import InternalWriter
from test.helper import check_content_files

TEST_PATH = os.path.dirname(os.path.realpath(__file__))

def _test(file_source_path, mode_data, mode_sequence):
    learningset = InternalParser().parse_from_path(file_source_path)
    destination_path = os.path.join(TEST_PATH, 'new')
    os.makedirs(destination_path, exist_ok=True)
    if (mode_data is "json"):
        file_destination_path = os.path.join(destination_path, 'profile.json')
    else:
        file_destination_path = os.path.join(destination_path, learningset.name + ".xlsx")
    InternalWriter().write(learningset, destination_path, mode_data=mode_data, mode_sequence=mode_sequence)
    check_content_files(file_source_path, file_destination_path)
    #shutil.rmtree(destination_path)

def test_internal():
    _test(os.path.join(TEST_PATH, 'data', 'internal', 'BirdChicken', 'profile.json'), 'json', 'auto')

def test_internal_example():
    _test(os.path.join(TEST_PATH, 'data', 'internal', 'example', 'profile.json'), 'json', 'auto')

def test_internal_xslsx():
    _test(os.path.join(TEST_PATH, 'data', 'internal', 'examplexlsx', 'example.xlsx'), 'json', 'auto')


def test_internal_example_to_json_daf():
    _test(os.path.join(TEST_PATH, 'data', 'internal', 'example', 'profile.json'), 'json', 'daf')

def test_internal_example_to_xlsx_daf():
    _test(os.path.join(TEST_PATH, 'data', 'internal', 'example', 'profile.json'), 'spreadsheet_without_data', 'daf')

def test_internal_example_to_xlsx_by_obs():
    _test(os.path.join(TEST_PATH, 'data', 'internal', 'example', 'profile.json'), 'spreadsheet_with_data', 'observation')

def test_internal_example_to_xlsx_by_seq():
    _test(os.path.join(TEST_PATH, 'data', 'internal', 'example', 'profile.json'), 'spreadsheet_without_data', 'sequence')

def test_internal_example_to_json_by_obs():
    _test(os.path.join(TEST_PATH, 'data', 'internal', 'example', 'profile.json'), 'json', 'observation')

def test_internal_example_to_json_by_seq():
    _test(os.path.join(TEST_PATH, 'data', 'internal', 'example', 'profile.json'), 'json', 'sequence')