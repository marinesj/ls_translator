# coding=UTF-8

"""
    Main script
"""

import sys
import os

#from memory_profiler import profile
# add package in the syspath
sys.path.insert(0, os.path.join(os.path.abspath('.')))

from test import test_internal, test_orange, test_ucr, test_arff, test_old

PATH = os.path.dirname(os.path.realpath(__file__))


#@profile
def main():
    # internal format
    test_internal.test_internal()
    # orange 2 format
    test_orange.test_orange_2()
    # orange 3 format
    test_orange.test_orange_3()
    # orange 2 format with internal format as transition format
    test_orange.test_orange_2(True)
    # orange 3 format with internal format as transition format
    test_orange.test_orange_3(True)
    # ucr format
    test_ucr.test_ucr()
    # arff
    test_arff.test_arff()
    # old
    test_old.test_old()

if __name__ == "__main__":
    main()
